# CompactControl

[!["Latest Release"](https://img.shields.io/badge/Release-v1.3.22-9cf.svg)](https://github.com/saeedmhq/CompactControl/releases/latest)

## Digital remote control for Compact LINAC system

> This app would only work with our CompactControl board and interface
