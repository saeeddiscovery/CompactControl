﻿namespace Compact_Control
{
    partial class Form_Fields
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn_editField = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbl_Y2 = new System.Windows.Forms.Label();
            this.lbl_Y1 = new System.Windows.Forms.Label();
            this.lbl_X2 = new System.Windows.Forms.Label();
            this.lbl_X1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lbl_Coli = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lbl_Gantry = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.btn_deleteField = new System.Windows.Forms.Button();
            this.picBtn_AcceptPatient = new System.Windows.Forms.PictureBox();
            this.btn_AddField = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lbl_shadowTray = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_Bolous = new System.Windows.Forms.Label();
            this.lbl_Wedge = new System.Windows.Forms.Label();
            this.lbl_MU = new System.Windows.Forms.Label();
            this.lbl_Dose = new System.Windows.Forms.Label();
            this.lbl_ssd = new System.Windows.Forms.Label();
            this.lbl_Site = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lbl_FName = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dataGridView_Fields = new System.Windows.Forms.DataGridView();
            this.picBtnToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lbl_Dr = new System.Windows.Forms.Label();
            this.lbl_PName = new System.Windows.Forms.Label();
            this.lbl_PID = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lbl_Long = new System.Windows.Forms.Label();
            this.lbl_Lat = new System.Windows.Forms.Label();
            this.lbl_Column = new System.Windows.Forms.Label();
            this.lbl_Vert = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lbl_Iso = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Colimator = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBtn_AcceptPatient)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Fields)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.groupBox5);
            this.groupBox3.Controls.Add(this.btn_editField);
            this.groupBox3.Controls.Add(this.groupBox1);
            this.groupBox3.Controls.Add(this.btn_deleteField);
            this.groupBox3.Controls.Add(this.picBtn_AcceptPatient);
            this.groupBox3.Controls.Add(this.btn_AddField);
            this.groupBox3.Controls.Add(this.groupBox2);
            this.groupBox3.Controls.Add(this.dataGridView_Fields);
            this.groupBox3.Location = new System.Drawing.Point(12, 120);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(460, 569);
            this.groupBox3.TabIndex = 18;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Fields";
            // 
            // btn_editField
            // 
            this.btn_editField.ForeColor = System.Drawing.Color.Black;
            this.btn_editField.Location = new System.Drawing.Point(194, 519);
            this.btn_editField.Name = "btn_editField";
            this.btn_editField.Size = new System.Drawing.Size(82, 41);
            this.btn_editField.TabIndex = 17;
            this.btn_editField.Text = "Edit Field...";
            this.btn_editField.UseVisualStyleBackColor = true;
            this.btn_editField.Click += new System.EventHandler(this.btn_editField_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbl_Y2);
            this.groupBox1.Controls.Add(this.lbl_Y1);
            this.groupBox1.Controls.Add(this.lbl_X2);
            this.groupBox1.Controls.Add(this.lbl_X1);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.lbl_Coli);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.lbl_Gantry);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox1.ForeColor = System.Drawing.Color.DimGray;
            this.groupBox1.Location = new System.Drawing.Point(18, 378);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(424, 102);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Set Values";
            // 
            // lbl_Y2
            // 
            this.lbl_Y2.AutoSize = true;
            this.lbl_Y2.ForeColor = System.Drawing.Color.SlateBlue;
            this.lbl_Y2.Location = new System.Drawing.Point(337, 65);
            this.lbl_Y2.Name = "lbl_Y2";
            this.lbl_Y2.Size = new System.Drawing.Size(13, 17);
            this.lbl_Y2.TabIndex = 14;
            this.lbl_Y2.Text = "-";
            // 
            // lbl_Y1
            // 
            this.lbl_Y1.AutoSize = true;
            this.lbl_Y1.ForeColor = System.Drawing.Color.SlateBlue;
            this.lbl_Y1.Location = new System.Drawing.Point(337, 31);
            this.lbl_Y1.Name = "lbl_Y1";
            this.lbl_Y1.Size = new System.Drawing.Size(13, 17);
            this.lbl_Y1.TabIndex = 13;
            this.lbl_Y1.Text = "-";
            // 
            // lbl_X2
            // 
            this.lbl_X2.AutoSize = true;
            this.lbl_X2.ForeColor = System.Drawing.Color.SlateBlue;
            this.lbl_X2.Location = new System.Drawing.Point(206, 65);
            this.lbl_X2.Name = "lbl_X2";
            this.lbl_X2.Size = new System.Drawing.Size(13, 17);
            this.lbl_X2.TabIndex = 11;
            this.lbl_X2.Text = "-";
            // 
            // lbl_X1
            // 
            this.lbl_X1.AutoSize = true;
            this.lbl_X1.ForeColor = System.Drawing.Color.SlateBlue;
            this.lbl_X1.Location = new System.Drawing.Point(206, 31);
            this.lbl_X1.Name = "lbl_X1";
            this.lbl_X1.Size = new System.Drawing.Size(13, 17);
            this.lbl_X1.TabIndex = 10;
            this.lbl_X1.Text = "-";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(303, 65);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 17);
            this.label13.TabIndex = 9;
            this.label13.Text = "Y2:";
            // 
            // lbl_Coli
            // 
            this.lbl_Coli.AutoSize = true;
            this.lbl_Coli.ForeColor = System.Drawing.Color.SlateBlue;
            this.lbl_Coli.Location = new System.Drawing.Point(85, 65);
            this.lbl_Coli.Name = "lbl_Coli";
            this.lbl_Coli.Size = new System.Drawing.Size(13, 17);
            this.lbl_Coli.TabIndex = 8;
            this.lbl_Coli.Text = "-";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(303, 31);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(29, 17);
            this.label17.TabIndex = 8;
            this.label17.Text = "Y1:";
            // 
            // lbl_Gantry
            // 
            this.lbl_Gantry.AutoSize = true;
            this.lbl_Gantry.ForeColor = System.Drawing.Color.SlateBlue;
            this.lbl_Gantry.Location = new System.Drawing.Point(85, 31);
            this.lbl_Gantry.Name = "lbl_Gantry";
            this.lbl_Gantry.Size = new System.Drawing.Size(13, 17);
            this.lbl_Gantry.TabIndex = 7;
            this.lbl_Gantry.Text = "-";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(174, 65);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(29, 17);
            this.label20.TabIndex = 6;
            this.label20.Text = "X2:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(174, 31);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(29, 17);
            this.label21.TabIndex = 5;
            this.label21.Text = "X1:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(17, 65);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(71, 17);
            this.label23.TabIndex = 2;
            this.label23.Text = "Colimator:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(17, 31);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(55, 17);
            this.label24.TabIndex = 1;
            this.label24.Text = "Gantry:";
            // 
            // btn_deleteField
            // 
            this.btn_deleteField.ForeColor = System.Drawing.Color.Black;
            this.btn_deleteField.Location = new System.Drawing.Point(106, 519);
            this.btn_deleteField.Name = "btn_deleteField";
            this.btn_deleteField.Size = new System.Drawing.Size(82, 41);
            this.btn_deleteField.TabIndex = 16;
            this.btn_deleteField.Text = "Delete Field";
            this.btn_deleteField.UseVisualStyleBackColor = true;
            this.btn_deleteField.Visible = false;
            this.btn_deleteField.Click += new System.EventHandler(this.btn_deleteField_Click);
            // 
            // picBtn_AcceptPatient
            // 
            this.picBtn_AcceptPatient.BackgroundImage = global::Compact_Control.Properties.Resources.btn_AcceptPatient_Disabled;
            this.picBtn_AcceptPatient.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picBtn_AcceptPatient.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picBtn_AcceptPatient.Location = new System.Drawing.Point(366, 488);
            this.picBtn_AcceptPatient.Name = "picBtn_AcceptPatient";
            this.picBtn_AcceptPatient.Size = new System.Drawing.Size(76, 72);
            this.picBtn_AcceptPatient.TabIndex = 6;
            this.picBtn_AcceptPatient.TabStop = false;
            this.picBtn_AcceptPatient.Click += new System.EventHandler(this.picBtn_AcceptPatient_Click);
            this.picBtn_AcceptPatient.MouseEnter += new System.EventHandler(this.picBtn_AcceptPatient_MouseEnter);
            this.picBtn_AcceptPatient.MouseLeave += new System.EventHandler(this.picBtn_AcceptPatient_MouseLeave);
            // 
            // btn_AddField
            // 
            this.btn_AddField.ForeColor = System.Drawing.Color.Black;
            this.btn_AddField.Location = new System.Drawing.Point(18, 519);
            this.btn_AddField.Name = "btn_AddField";
            this.btn_AddField.Size = new System.Drawing.Size(82, 41);
            this.btn_AddField.TabIndex = 15;
            this.btn_AddField.Text = "Add Field...";
            this.btn_AddField.UseVisualStyleBackColor = true;
            this.btn_AddField.Visible = false;
            this.btn_AddField.Click += new System.EventHandler(this.btn_AddField_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbl_shadowTray);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.lbl_Bolous);
            this.groupBox2.Controls.Add(this.lbl_Wedge);
            this.groupBox2.Controls.Add(this.lbl_MU);
            this.groupBox2.Controls.Add(this.lbl_Dose);
            this.groupBox2.Controls.Add(this.lbl_ssd);
            this.groupBox2.Controls.Add(this.lbl_Site);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.lbl_FName);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox2.ForeColor = System.Drawing.Color.DimGray;
            this.groupBox2.Location = new System.Drawing.Point(18, 144);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(424, 122);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Selected Field";
            // 
            // lbl_shadowTray
            // 
            this.lbl_shadowTray.AutoSize = true;
            this.lbl_shadowTray.ForeColor = System.Drawing.Color.SlateBlue;
            this.lbl_shadowTray.Location = new System.Drawing.Point(378, 55);
            this.lbl_shadowTray.Name = "lbl_shadowTray";
            this.lbl_shadowTray.Size = new System.Drawing.Size(13, 17);
            this.lbl_shadowTray.TabIndex = 16;
            this.lbl_shadowTray.Text = "-";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(282, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 17);
            this.label2.TabIndex = 15;
            this.label2.Text = "Shadow Tray:";
            // 
            // lbl_Bolous
            // 
            this.lbl_Bolous.AutoSize = true;
            this.lbl_Bolous.ForeColor = System.Drawing.Color.SlateBlue;
            this.lbl_Bolous.Location = new System.Drawing.Point(378, 88);
            this.lbl_Bolous.Name = "lbl_Bolous";
            this.lbl_Bolous.Size = new System.Drawing.Size(13, 17);
            this.lbl_Bolous.TabIndex = 14;
            this.lbl_Bolous.Text = "-";
            // 
            // lbl_Wedge
            // 
            this.lbl_Wedge.AutoSize = true;
            this.lbl_Wedge.ForeColor = System.Drawing.Color.SlateBlue;
            this.lbl_Wedge.Location = new System.Drawing.Point(378, 22);
            this.lbl_Wedge.Name = "lbl_Wedge";
            this.lbl_Wedge.Size = new System.Drawing.Size(13, 17);
            this.lbl_Wedge.TabIndex = 13;
            this.lbl_Wedge.Text = "-";
            // 
            // lbl_MU
            // 
            this.lbl_MU.AutoSize = true;
            this.lbl_MU.ForeColor = System.Drawing.Color.SlateBlue;
            this.lbl_MU.Location = new System.Drawing.Point(223, 88);
            this.lbl_MU.Name = "lbl_MU";
            this.lbl_MU.Size = new System.Drawing.Size(13, 17);
            this.lbl_MU.TabIndex = 12;
            this.lbl_MU.Text = "-";
            // 
            // lbl_Dose
            // 
            this.lbl_Dose.AutoSize = true;
            this.lbl_Dose.ForeColor = System.Drawing.Color.SlateBlue;
            this.lbl_Dose.Location = new System.Drawing.Point(223, 55);
            this.lbl_Dose.Name = "lbl_Dose";
            this.lbl_Dose.Size = new System.Drawing.Size(13, 17);
            this.lbl_Dose.TabIndex = 11;
            this.lbl_Dose.Text = "-";
            // 
            // lbl_ssd
            // 
            this.lbl_ssd.AutoSize = true;
            this.lbl_ssd.ForeColor = System.Drawing.Color.SlateBlue;
            this.lbl_ssd.Location = new System.Drawing.Point(223, 22);
            this.lbl_ssd.Name = "lbl_ssd";
            this.lbl_ssd.Size = new System.Drawing.Size(13, 17);
            this.lbl_ssd.TabIndex = 10;
            this.lbl_ssd.Text = "-";
            // 
            // lbl_Site
            // 
            this.lbl_Site.AutoSize = true;
            this.lbl_Site.ForeColor = System.Drawing.Color.SlateBlue;
            this.lbl_Site.Location = new System.Drawing.Point(70, 55);
            this.lbl_Site.Name = "lbl_Site";
            this.lbl_Site.Size = new System.Drawing.Size(13, 17);
            this.lbl_Site.TabIndex = 9;
            this.lbl_Site.Text = "-";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(282, 88);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(70, 17);
            this.label14.TabIndex = 9;
            this.label14.Text = "BOLOUS:";
            // 
            // lbl_FName
            // 
            this.lbl_FName.AutoSize = true;
            this.lbl_FName.ForeColor = System.Drawing.Color.SlateBlue;
            this.lbl_FName.Location = new System.Drawing.Point(70, 22);
            this.lbl_FName.Name = "lbl_FName";
            this.lbl_FName.Size = new System.Drawing.Size(13, 17);
            this.lbl_FName.TabIndex = 8;
            this.lbl_FName.Text = "-";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(282, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(64, 17);
            this.label15.TabIndex = 8;
            this.label15.Text = "WEDGE:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(174, 88);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 17);
            this.label10.TabIndex = 7;
            this.label10.Text = "MU:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(174, 55);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 17);
            this.label11.TabIndex = 6;
            this.label11.Text = "DOSE:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(174, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 17);
            this.label12.TabIndex = 5;
            this.label12.Text = "SSD:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 55);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 17);
            this.label9.TabIndex = 4;
            this.label9.Text = "SITE:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 17);
            this.label7.TabIndex = 2;
            this.label7.Text = "Name:";
            // 
            // dataGridView_Fields
            // 
            this.dataGridView_Fields.AllowUserToAddRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.dataGridView_Fields.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView_Fields.BackgroundColor = System.Drawing.Color.Snow;
            this.dataGridView_Fields.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Fields.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column11,
            this.Column10,
            this.Column12,
            this.Column13,
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column1,
            this.Colimator,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5});
            this.dataGridView_Fields.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView_Fields.GridColor = System.Drawing.Color.Orange;
            this.dataGridView_Fields.Location = new System.Drawing.Point(18, 18);
            this.dataGridView_Fields.MultiSelect = false;
            this.dataGridView_Fields.Name = "dataGridView_Fields";
            this.dataGridView_Fields.ReadOnly = true;
            this.dataGridView_Fields.RowHeadersVisible = false;
            this.dataGridView_Fields.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_Fields.ShowEditingIcon = false;
            this.dataGridView_Fields.Size = new System.Drawing.Size(424, 121);
            this.dataGridView_Fields.TabIndex = 1;
            this.dataGridView_Fields.SelectionChanged += new System.EventHandler(this.dataGridView2_SelectionChanged);
            this.dataGridView_Fields.DoubleClick += new System.EventHandler(this.dataGridView_Fields_DoubleClick);
            this.dataGridView_Fields.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dataGridView_Fields_KeyPress);
            // 
            // picBtnToolTip
            // 
            this.picBtnToolTip.AutoPopDelay = 5000;
            this.picBtnToolTip.BackColor = System.Drawing.Color.White;
            this.picBtnToolTip.InitialDelay = 500;
            this.picBtnToolTip.ReshowDelay = 100;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lbl_Dr);
            this.groupBox4.Controls.Add(this.lbl_PName);
            this.groupBox4.Controls.Add(this.lbl_PID);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox4.ForeColor = System.Drawing.Color.DimGray;
            this.groupBox4.Location = new System.Drawing.Point(12, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(460, 102);
            this.groupBox4.TabIndex = 17;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Patient Information";
            // 
            // lbl_Dr
            // 
            this.lbl_Dr.AutoSize = true;
            this.lbl_Dr.ForeColor = System.Drawing.Color.SlateBlue;
            this.lbl_Dr.Location = new System.Drawing.Point(93, 74);
            this.lbl_Dr.Name = "lbl_Dr";
            this.lbl_Dr.Size = new System.Drawing.Size(13, 17);
            this.lbl_Dr.TabIndex = 6;
            this.lbl_Dr.Text = "-";
            // 
            // lbl_PName
            // 
            this.lbl_PName.AutoSize = true;
            this.lbl_PName.ForeColor = System.Drawing.Color.SlateBlue;
            this.lbl_PName.Location = new System.Drawing.Point(93, 49);
            this.lbl_PName.Name = "lbl_PName";
            this.lbl_PName.Size = new System.Drawing.Size(13, 17);
            this.lbl_PName.TabIndex = 5;
            this.lbl_PName.Text = "-";
            // 
            // lbl_PID
            // 
            this.lbl_PID.AutoSize = true;
            this.lbl_PID.ForeColor = System.Drawing.Color.SlateBlue;
            this.lbl_PID.Location = new System.Drawing.Point(93, 26);
            this.lbl_PID.Name = "lbl_PID";
            this.lbl_PID.Size = new System.Drawing.Size(13, 17);
            this.lbl_PID.TabIndex = 4;
            this.lbl_PID.Text = "-";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 74);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 17);
            this.label8.TabIndex = 3;
            this.label8.Text = "Dr.:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 17);
            this.label6.TabIndex = 1;
            this.label6.Text = "Name:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Patient ID:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lbl_Long);
            this.groupBox5.Controls.Add(this.lbl_Lat);
            this.groupBox5.Controls.Add(this.lbl_Column);
            this.groupBox5.Controls.Add(this.lbl_Vert);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this.lbl_Iso);
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Controls.Add(this.label28);
            this.groupBox5.Controls.Add(this.label29);
            this.groupBox5.Controls.Add(this.label30);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox5.ForeColor = System.Drawing.Color.DimGray;
            this.groupBox5.Location = new System.Drawing.Point(18, 270);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(424, 102);
            this.groupBox5.TabIndex = 16;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Couch";
            // 
            // lbl_Long
            // 
            this.lbl_Long.AutoSize = true;
            this.lbl_Long.ForeColor = System.Drawing.Color.SlateBlue;
            this.lbl_Long.Location = new System.Drawing.Point(333, 65);
            this.lbl_Long.Name = "lbl_Long";
            this.lbl_Long.Size = new System.Drawing.Size(13, 17);
            this.lbl_Long.TabIndex = 13;
            this.lbl_Long.Text = "-";
            // 
            // lbl_Lat
            // 
            this.lbl_Lat.AutoSize = true;
            this.lbl_Lat.ForeColor = System.Drawing.Color.SlateBlue;
            this.lbl_Lat.Location = new System.Drawing.Point(233, 65);
            this.lbl_Lat.Name = "lbl_Lat";
            this.lbl_Lat.Size = new System.Drawing.Size(13, 17);
            this.lbl_Lat.TabIndex = 11;
            this.lbl_Lat.Text = "-";
            // 
            // lbl_Column
            // 
            this.lbl_Column.AutoSize = true;
            this.lbl_Column.ForeColor = System.Drawing.Color.SlateBlue;
            this.lbl_Column.Location = new System.Drawing.Point(233, 31);
            this.lbl_Column.Name = "lbl_Column";
            this.lbl_Column.Size = new System.Drawing.Size(13, 17);
            this.lbl_Column.TabIndex = 10;
            this.lbl_Column.Text = "-";
            // 
            // lbl_Vert
            // 
            this.lbl_Vert.AutoSize = true;
            this.lbl_Vert.ForeColor = System.Drawing.Color.SlateBlue;
            this.lbl_Vert.Location = new System.Drawing.Point(70, 65);
            this.lbl_Vert.Name = "lbl_Vert";
            this.lbl_Vert.Size = new System.Drawing.Size(13, 17);
            this.lbl_Vert.TabIndex = 8;
            this.lbl_Vert.Text = "-";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(282, 65);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(44, 17);
            this.label25.TabIndex = 8;
            this.label25.Text = "Long:";
            // 
            // lbl_Iso
            // 
            this.lbl_Iso.AutoSize = true;
            this.lbl_Iso.ForeColor = System.Drawing.Color.SlateBlue;
            this.lbl_Iso.Location = new System.Drawing.Point(70, 31);
            this.lbl_Iso.Name = "lbl_Iso";
            this.lbl_Iso.Size = new System.Drawing.Size(13, 17);
            this.lbl_Iso.TabIndex = 7;
            this.lbl_Iso.Text = "-";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(174, 65);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(32, 17);
            this.label27.TabIndex = 6;
            this.label27.Text = "Lat:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(174, 31);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(59, 17);
            this.label28.TabIndex = 5;
            this.label28.Text = "Column:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(17, 65);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(38, 17);
            this.label29.TabIndex = 2;
            this.label29.Text = "Vert:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(17, 31);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(30, 17);
            this.label30.TabIndex = 1;
            this.label30.Text = "Iso:";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Field Name";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 135;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "SITE";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "SSD";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "DOSE";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "MU";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "WEDGE";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // Column11
            // 
            this.Column11.HeaderText = "Shadow Tray";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "BOLOUS";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // Column12
            // 
            this.Column12.HeaderText = "Iso";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            // 
            // Column13
            // 
            this.Column13.HeaderText = "Column";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            // 
            // Column14
            // 
            this.Column14.HeaderText = "Vert";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            // 
            // Column15
            // 
            this.Column15.HeaderText = "Lat";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            // 
            // Column16
            // 
            this.Column16.HeaderText = "Long";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Gantry";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Colimator
            // 
            this.Colimator.HeaderText = "Colimator";
            this.Colimator.Name = "Colimator";
            this.Colimator.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "X1";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "X2";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Y1";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Y2";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Form_Fields
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 701);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form_Fields";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fields";
            this.Activated += new System.EventHandler(this.Form_Fields_Activated);
            this.Load += new System.EventHandler(this.Form_Fields_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBtn_AcceptPatient)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Fields)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn_deleteField;
        private System.Windows.Forms.PictureBox picBtn_AcceptPatient;
        private System.Windows.Forms.Button btn_AddField;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lbl_Bolous;
        private System.Windows.Forms.Label lbl_Wedge;
        private System.Windows.Forms.Label lbl_MU;
        private System.Windows.Forms.Label lbl_Dose;
        private System.Windows.Forms.Label lbl_ssd;
        private System.Windows.Forms.Label lbl_Site;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lbl_FName;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbl_Y2;
        private System.Windows.Forms.Label lbl_Y1;
        private System.Windows.Forms.Label lbl_X2;
        private System.Windows.Forms.Label lbl_X1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lbl_Coli;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lbl_Gantry;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ToolTip picBtnToolTip;
        private System.Windows.Forms.Label lbl_shadowTray;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lbl_Dr;
        private System.Windows.Forms.Label lbl_PName;
        private System.Windows.Forms.Label lbl_PID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.DataGridView dataGridView_Fields;
        private System.Windows.Forms.Button btn_editField;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label lbl_Long;
        private System.Windows.Forms.Label lbl_Lat;
        private System.Windows.Forms.Label lbl_Column;
        private System.Windows.Forms.Label lbl_Vert;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label lbl_Iso;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Colimator;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
    }
}