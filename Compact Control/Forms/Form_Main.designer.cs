﻿namespace Compact_Control
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.adcheck = new System.Windows.Forms.TextBox();
            this.tb_y2_co = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_y1_co = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_x1_co = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_x2_co = new System.Windows.Forms.TextBox();
            this.btn_start_stop = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.btn_y2_in = new System.Windows.Forms.Button();
            this.btn_gant_cw = new System.Windows.Forms.Button();
            this.btn_y1_out = new System.Windows.Forms.Button();
            this.trackBar_y2 = new System.Windows.Forms.TrackBar();
            this.btn_y1_in = new System.Windows.Forms.Button();
            this.btn_gant_ccw = new System.Windows.Forms.Button();
            this.btn_y2_out = new System.Windows.Forms.Button();
            this.trackBar_y1 = new System.Windows.Forms.TrackBar();
            this.btn_x2_out = new System.Windows.Forms.Button();
            this.btn_coli_cw = new System.Windows.Forms.Button();
            this.btn_x2_in = new System.Windows.Forms.Button();
            this.trackBar_x2 = new System.Windows.Forms.TrackBar();
            this.trackBar_gant = new System.Windows.Forms.TrackBar();
            this.btn_coli_ccw = new System.Windows.Forms.Button();
            this.btn_x1_out = new System.Windows.Forms.Button();
            this.trackBar_x1 = new System.Windows.Forms.TrackBar();
            this.trackBar_coli = new System.Windows.Forms.TrackBar();
            this.btn_x1_in = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.btn_cancelLearn = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btn_learn = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button16 = new System.Windows.Forms.Button();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btn_edit = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tb_coli_flen = new System.Windows.Forms.TextBox();
            this.tb_coli_len = new System.Windows.Forms.TextBox();
            this.tb_coli_zpnt = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.tb_gant_flen = new System.Windows.Forms.TextBox();
            this.tb_gant_len = new System.Windows.Forms.TextBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.label45 = new System.Windows.Forms.Label();
            this.textBox53 = new System.Windows.Forms.TextBox();
            this.textBox54 = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.textBox51 = new System.Windows.Forms.TextBox();
            this.textBox52 = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.textBox50 = new System.Windows.Forms.TextBox();
            this.textBox49 = new System.Windows.Forms.TextBox();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.tb_y2_offset = new System.Windows.Forms.TextBox();
            this.tb_gant_zpnt = new System.Windows.Forms.TextBox();
            this.tb_y1_offset = new System.Windows.Forms.TextBox();
            this.tb_x2_offset = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.tb_x1_offset = new System.Windows.Forms.TextBox();
            this.tb_coli_offset = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.tb_gant_offset = new System.Windows.Forms.TextBox();
            this.tb_y2_gain = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.tb_y1_gain = new System.Windows.Forms.TextBox();
            this.tb_x2_gain = new System.Windows.Forms.TextBox();
            this.tb_x1_gain = new System.Windows.Forms.TextBox();
            this.tb_coli_gain = new System.Windows.Forms.TextBox();
            this.tb_gant_gain = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.pb_receiveStatus = new System.Windows.Forms.PictureBox();
            this.label72 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.tb_x1_set = new System.Windows.Forms.TextBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.tb_x2_set = new System.Windows.Forms.TextBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label33 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.tb_coli_set = new System.Windows.Forms.TextBox();
            this.tb_y1_set = new System.Windows.Forms.TextBox();
            this.tb_gant_set = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.tb_y2_set = new System.Windows.Forms.TextBox();
            this.btn_clearTerminal_oth = new System.Windows.Forms.Button();
            this.label64 = new System.Windows.Forms.Label();
            this.lbl_in_cnt = new System.Windows.Forms.Label();
            this.btn_clearTerminal_in = new System.Windows.Forms.Button();
            this.tb_terminal_oth = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.tb_terminal_in = new System.Windows.Forms.TextBox();
            this.btn_clearTerminal = new System.Windows.Forms.Button();
            this.label48 = new System.Windows.Forms.Label();
            this.tb_terminal_out = new System.Windows.Forms.TextBox();
            this.gb_parameters = new System.Windows.Forms.GroupBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.btn_saveParameters = new System.Windows.Forms.Button();
            this.textBox79 = new System.Windows.Forms.TextBox();
            this.textBox80 = new System.Windows.Forms.TextBox();
            this.textBox81 = new System.Windows.Forms.TextBox();
            this.textBox82 = new System.Windows.Forms.TextBox();
            this.textBox83 = new System.Windows.Forms.TextBox();
            this.textBox84 = new System.Windows.Forms.TextBox();
            this.textBox85 = new System.Windows.Forms.TextBox();
            this.textBox86 = new System.Windows.Forms.TextBox();
            this.textBox87 = new System.Windows.Forms.TextBox();
            this.textBox88 = new System.Windows.Forms.TextBox();
            this.textBox89 = new System.Windows.Forms.TextBox();
            this.textBox90 = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.textBox55 = new System.Windows.Forms.TextBox();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.textBox60 = new System.Windows.Forms.TextBox();
            this.textBox61 = new System.Windows.Forms.TextBox();
            this.textBox62 = new System.Windows.Forms.TextBox();
            this.textBox63 = new System.Windows.Forms.TextBox();
            this.textBox64 = new System.Windows.Forms.TextBox();
            this.textBox77 = new System.Windows.Forms.TextBox();
            this.textBox78 = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.textBox65 = new System.Windows.Forms.TextBox();
            this.textBox66 = new System.Windows.Forms.TextBox();
            this.textBox67 = new System.Windows.Forms.TextBox();
            this.textBox68 = new System.Windows.Forms.TextBox();
            this.textBox69 = new System.Windows.Forms.TextBox();
            this.textBox70 = new System.Windows.Forms.TextBox();
            this.textBox71 = new System.Windows.Forms.TextBox();
            this.textBox72 = new System.Windows.Forms.TextBox();
            this.textBox73 = new System.Windows.Forms.TextBox();
            this.textBox74 = new System.Windows.Forms.TextBox();
            this.textBox75 = new System.Windows.Forms.TextBox();
            this.textBox76 = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.lbl_out_cnt = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panel_AdminControls = new System.Windows.Forms.Panel();
            this.panel_Toolbar = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.picBtn_Exit = new System.Windows.Forms.PictureBox();
            this.picBtn_LogOff = new System.Windows.Forms.PictureBox();
            this.picBtn_Setting = new System.Windows.Forms.PictureBox();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.picBtn_Restart = new System.Windows.Forms.PictureBox();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.picBtn_Shutdown = new System.Windows.Forms.PictureBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.picBtn_Connect = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_init = new System.Windows.Forms.Label();
            this.label_date = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label_time = new System.Windows.Forms.Label();
            this.label_ConnectStatus = new System.Windows.Forms.Label();
            this.label_title = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.picBtn_Close = new System.Windows.Forms.PictureBox();
            this.picBtnToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.label39 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel_ClientControls = new System.Windows.Forms.Panel();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.timer4 = new System.Windows.Forms.Timer(this.components);
            this.timer5 = new System.Windows.Forms.Timer(this.components);
            this.panel_status = new System.Windows.Forms.Panel();
            this.label_upTime = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label_ram = new System.Windows.Forms.Label();
            this.lbl_version = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label_cpu = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_y2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_y1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_x2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_gant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_x1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_coli)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_receiveStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.gb_parameters.SuspendLayout();
            this.panel_AdminControls.SuspendLayout();
            this.panel_Toolbar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBtn_Exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBtn_LogOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBtn_Setting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBtn_Restart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBtn_Shutdown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBtn_Connect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBtn_Close)).BeginInit();
            this.panel_ClientControls.SuspendLayout();
            this.panel_status.SuspendLayout();
            this.SuspendLayout();
            // 
            // serialPort1
            // 
            this.serialPort1.BaudRate = 57600;
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived_1);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 50;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1122, 636);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabPage1.Controls.Add(this.splitContainer1);
            this.tabPage1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tabPage1.Location = new System.Drawing.Point(4, 27);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1114, 605);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Diagnose";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox5);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox6);
            this.splitContainer1.Panel1MinSize = 500;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.btn_start_stop);
            this.splitContainer1.Panel2.Controls.Add(this.groupBox7);
            this.splitContainer1.Size = new System.Drawing.Size(1108, 599);
            this.splitContainer1.SplitterDistance = 500;
            this.splitContainer1.TabIndex = 86;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.textBox7);
            this.groupBox5.Controls.Add(this.label38);
            this.groupBox5.Controls.Add(this.textBox8);
            this.groupBox5.Controls.Add(this.textBox1);
            this.groupBox5.Controls.Add(this.textBox9);
            this.groupBox5.Controls.Add(this.textBox44);
            this.groupBox5.Controls.Add(this.textBox10);
            this.groupBox5.Controls.Add(this.textBox2);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.textBox43);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox5.Location = new System.Drawing.Point(15, 48);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(480, 120);
            this.groupBox5.TabIndex = 86;
            this.groupBox5.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.Location = new System.Drawing.Point(13, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 18);
            this.label2.TabIndex = 49;
            this.label2.Text = "Collimator Angle";
            // 
            // textBox7
            // 
            this.textBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox7.Location = new System.Drawing.Point(228, 46);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(72, 24);
            this.textBox7.TabIndex = 2;
            this.textBox7.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label38.Location = new System.Drawing.Point(388, 17);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(70, 18);
            this.label38.TabIndex = 85;
            this.label38.Text = "Outcome";
            // 
            // textBox8
            // 
            this.textBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox8.Location = new System.Drawing.Point(308, 46);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(72, 24);
            this.textBox8.TabIndex = 3;
            this.textBox8.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox1.Location = new System.Drawing.Point(148, 46);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(72, 24);
            this.textBox1.TabIndex = 1;
            this.textBox1.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // textBox9
            // 
            this.textBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox9.Location = new System.Drawing.Point(228, 86);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(72, 24);
            this.textBox9.TabIndex = 6;
            this.textBox9.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // textBox44
            // 
            this.textBox44.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox44.Location = new System.Drawing.Point(388, 86);
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(72, 24);
            this.textBox44.TabIndex = 8;
            this.textBox44.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // textBox10
            // 
            this.textBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox10.Location = new System.Drawing.Point(308, 86);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(72, 24);
            this.textBox10.TabIndex = 7;
            this.textBox10.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox2.Location = new System.Drawing.Point(148, 86);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(72, 24);
            this.textBox2.TabIndex = 5;
            this.textBox2.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label8.Location = new System.Drawing.Point(148, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 18);
            this.label8.TabIndex = 78;
            this.label8.Text = "Coarse";
            // 
            // textBox43
            // 
            this.textBox43.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox43.Location = new System.Drawing.Point(388, 46);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(72, 24);
            this.textBox43.TabIndex = 4;
            this.textBox43.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label1.Location = new System.Drawing.Point(13, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 18);
            this.label1.TabIndex = 48;
            this.label1.Text = "Gantry Angle";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label9.Location = new System.Drawing.Point(228, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 18);
            this.label9.TabIndex = 79;
            this.label9.Text = "Fine 1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label10.Location = new System.Drawing.Point(308, 17);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 18);
            this.label10.TabIndex = 80;
            this.label10.Text = "Fine 2";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label40);
            this.groupBox6.Controls.Add(this.label6);
            this.groupBox6.Controls.Add(this.adcheck);
            this.groupBox6.Controls.Add(this.tb_y2_co);
            this.groupBox6.Controls.Add(this.label5);
            this.groupBox6.Controls.Add(this.tb_y1_co);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.tb_x1_co);
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.tb_x2_co);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox6.Location = new System.Drawing.Point(15, 174);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(248, 218);
            this.groupBox6.TabIndex = 87;
            this.groupBox6.TabStop = false;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label40.Location = new System.Drawing.Point(13, 187);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(88, 18);
            this.label40.TabIndex = 55;
            this.label40.Text = "A/D Check";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label6.Location = new System.Drawing.Point(13, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 18);
            this.label6.TabIndex = 53;
            this.label6.Text = "Diaphragm Y2";
            // 
            // adcheck
            // 
            this.adcheck.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.adcheck.Location = new System.Drawing.Point(148, 183);
            this.adcheck.Name = "adcheck";
            this.adcheck.Size = new System.Drawing.Size(72, 24);
            this.adcheck.TabIndex = 13;
            this.adcheck.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // tb_y2_co
            // 
            this.tb_y2_co.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_y2_co.Location = new System.Drawing.Point(148, 63);
            this.tb_y2_co.Name = "tb_y2_co";
            this.tb_y2_co.Size = new System.Drawing.Size(72, 24);
            this.tb_y2_co.TabIndex = 10;
            this.tb_y2_co.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label5.Location = new System.Drawing.Point(13, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 18);
            this.label5.TabIndex = 52;
            this.label5.Text = "Diaphragm Y1";
            // 
            // tb_y1_co
            // 
            this.tb_y1_co.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_y1_co.Location = new System.Drawing.Point(148, 23);
            this.tb_y1_co.Name = "tb_y1_co";
            this.tb_y1_co.Size = new System.Drawing.Size(72, 24);
            this.tb_y1_co.TabIndex = 9;
            this.tb_y1_co.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label3.Location = new System.Drawing.Point(12, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 18);
            this.label3.TabIndex = 50;
            this.label3.Text = "Diaphragm X1";
            // 
            // tb_x1_co
            // 
            this.tb_x1_co.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_x1_co.Location = new System.Drawing.Point(147, 103);
            this.tb_x1_co.Name = "tb_x1_co";
            this.tb_x1_co.Size = new System.Drawing.Size(72, 24);
            this.tb_x1_co.TabIndex = 11;
            this.tb_x1_co.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label4.Location = new System.Drawing.Point(12, 147);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 18);
            this.label4.TabIndex = 51;
            this.label4.Text = "Diaphragm X2";
            // 
            // tb_x2_co
            // 
            this.tb_x2_co.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_x2_co.Location = new System.Drawing.Point(147, 143);
            this.tb_x2_co.Name = "tb_x2_co";
            this.tb_x2_co.Size = new System.Drawing.Size(72, 24);
            this.tb_x2_co.TabIndex = 12;
            this.tb_x2_co.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // btn_start_stop
            // 
            this.btn_start_stop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btn_start_stop.Location = new System.Drawing.Point(10, 356);
            this.btn_start_stop.Name = "btn_start_stop";
            this.btn_start_stop.Size = new System.Drawing.Size(161, 30);
            this.btn_start_stop.TabIndex = 81;
            this.btn_start_stop.Text = "Start";
            this.btn_start_stop.UseVisualStyleBackColor = true;
            this.btn_start_stop.Click += new System.EventHandler(this.button14_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.btn_y2_in);
            this.groupBox7.Controls.Add(this.btn_gant_cw);
            this.groupBox7.Controls.Add(this.btn_y1_out);
            this.groupBox7.Controls.Add(this.trackBar_y2);
            this.groupBox7.Controls.Add(this.btn_y1_in);
            this.groupBox7.Controls.Add(this.btn_gant_ccw);
            this.groupBox7.Controls.Add(this.btn_y2_out);
            this.groupBox7.Controls.Add(this.trackBar_y1);
            this.groupBox7.Controls.Add(this.btn_x2_out);
            this.groupBox7.Controls.Add(this.btn_coli_cw);
            this.groupBox7.Controls.Add(this.btn_x2_in);
            this.groupBox7.Controls.Add(this.trackBar_x2);
            this.groupBox7.Controls.Add(this.trackBar_gant);
            this.groupBox7.Controls.Add(this.btn_coli_ccw);
            this.groupBox7.Controls.Add(this.btn_x1_out);
            this.groupBox7.Controls.Add(this.trackBar_x1);
            this.groupBox7.Controls.Add(this.trackBar_coli);
            this.groupBox7.Controls.Add(this.btn_x1_in);
            this.groupBox7.Enabled = false;
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox7.Location = new System.Drawing.Point(6, 65);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(359, 289);
            this.groupBox7.TabIndex = 87;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Move";
            // 
            // btn_y2_in
            // 
            this.btn_y2_in.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btn_y2_in.Location = new System.Drawing.Point(4, 170);
            this.btn_y2_in.Name = "btn_y2_in";
            this.btn_y2_in.Size = new System.Drawing.Size(72, 30);
            this.btn_y2_in.TabIndex = 64;
            this.btn_y2_in.Text = "IN";
            this.btn_y2_in.UseVisualStyleBackColor = true;
            this.btn_y2_in.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button11_MouseDown);
            // 
            // btn_gant_cw
            // 
            this.btn_gant_cw.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btn_gant_cw.Location = new System.Drawing.Point(4, 27);
            this.btn_gant_cw.Name = "btn_gant_cw";
            this.btn_gant_cw.Size = new System.Drawing.Size(72, 30);
            this.btn_gant_cw.TabIndex = 55;
            this.btn_gant_cw.Text = "CW";
            this.btn_gant_cw.UseVisualStyleBackColor = true;
            this.btn_gant_cw.Click += new System.EventHandler(this.button2_Click);
            // 
            // btn_y1_out
            // 
            this.btn_y1_out.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btn_y1_out.Location = new System.Drawing.Point(93, 130);
            this.btn_y1_out.Name = "btn_y1_out";
            this.btn_y1_out.Size = new System.Drawing.Size(72, 30);
            this.btn_y1_out.TabIndex = 65;
            this.btn_y1_out.Text = "OUT";
            this.btn_y1_out.UseVisualStyleBackColor = true;
            this.btn_y1_out.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button12_MouseDown);
            // 
            // trackBar_y2
            // 
            this.trackBar_y2.Location = new System.Drawing.Point(181, 170);
            this.trackBar_y2.Maximum = 4;
            this.trackBar_y2.Minimum = 1;
            this.trackBar_y2.Name = "trackBar_y2";
            this.trackBar_y2.Size = new System.Drawing.Size(159, 45);
            this.trackBar_y2.TabIndex = 73;
            this.trackBar_y2.Value = 1;
            // 
            // btn_y1_in
            // 
            this.btn_y1_in.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btn_y1_in.Location = new System.Drawing.Point(4, 130);
            this.btn_y1_in.Name = "btn_y1_in";
            this.btn_y1_in.Size = new System.Drawing.Size(72, 30);
            this.btn_y1_in.TabIndex = 63;
            this.btn_y1_in.Text = "IN";
            this.btn_y1_in.UseVisualStyleBackColor = true;
            this.btn_y1_in.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button10_MouseDown);
            // 
            // btn_gant_ccw
            // 
            this.btn_gant_ccw.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btn_gant_ccw.Location = new System.Drawing.Point(93, 27);
            this.btn_gant_ccw.Name = "btn_gant_ccw";
            this.btn_gant_ccw.Size = new System.Drawing.Size(72, 30);
            this.btn_gant_ccw.TabIndex = 56;
            this.btn_gant_ccw.Text = "CCW";
            this.btn_gant_ccw.UseVisualStyleBackColor = true;
            this.btn_gant_ccw.Click += new System.EventHandler(this.button3_Click);
            // 
            // btn_y2_out
            // 
            this.btn_y2_out.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btn_y2_out.Location = new System.Drawing.Point(93, 170);
            this.btn_y2_out.Name = "btn_y2_out";
            this.btn_y2_out.Size = new System.Drawing.Size(72, 30);
            this.btn_y2_out.TabIndex = 66;
            this.btn_y2_out.Text = "OUT";
            this.btn_y2_out.UseVisualStyleBackColor = true;
            this.btn_y2_out.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button13_MouseDown);
            // 
            // trackBar_y1
            // 
            this.trackBar_y1.Location = new System.Drawing.Point(181, 130);
            this.trackBar_y1.Maximum = 4;
            this.trackBar_y1.Minimum = 1;
            this.trackBar_y1.Name = "trackBar_y1";
            this.trackBar_y1.Size = new System.Drawing.Size(159, 45);
            this.trackBar_y1.TabIndex = 72;
            this.trackBar_y1.Value = 1;
            // 
            // btn_x2_out
            // 
            this.btn_x2_out.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btn_x2_out.Location = new System.Drawing.Point(93, 250);
            this.btn_x2_out.Name = "btn_x2_out";
            this.btn_x2_out.Size = new System.Drawing.Size(72, 30);
            this.btn_x2_out.TabIndex = 62;
            this.btn_x2_out.Text = "OUT";
            this.btn_x2_out.UseVisualStyleBackColor = true;
            this.btn_x2_out.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button9_MouseDown);
            // 
            // btn_coli_cw
            // 
            this.btn_coli_cw.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btn_coli_cw.Location = new System.Drawing.Point(4, 65);
            this.btn_coli_cw.Name = "btn_coli_cw";
            this.btn_coli_cw.Size = new System.Drawing.Size(72, 30);
            this.btn_coli_cw.TabIndex = 57;
            this.btn_coli_cw.Text = "CW";
            this.btn_coli_cw.UseVisualStyleBackColor = true;
            this.btn_coli_cw.Click += new System.EventHandler(this.button4_Click);
            // 
            // btn_x2_in
            // 
            this.btn_x2_in.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btn_x2_in.Location = new System.Drawing.Point(4, 250);
            this.btn_x2_in.Name = "btn_x2_in";
            this.btn_x2_in.Size = new System.Drawing.Size(72, 30);
            this.btn_x2_in.TabIndex = 61;
            this.btn_x2_in.Text = "IN";
            this.btn_x2_in.UseVisualStyleBackColor = true;
            this.btn_x2_in.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button8_MouseDown);
            // 
            // trackBar_x2
            // 
            this.trackBar_x2.Location = new System.Drawing.Point(181, 250);
            this.trackBar_x2.Maximum = 4;
            this.trackBar_x2.Minimum = 1;
            this.trackBar_x2.Name = "trackBar_x2";
            this.trackBar_x2.Size = new System.Drawing.Size(159, 45);
            this.trackBar_x2.TabIndex = 71;
            this.trackBar_x2.Value = 1;
            // 
            // trackBar_gant
            // 
            this.trackBar_gant.Location = new System.Drawing.Point(181, 27);
            this.trackBar_gant.Maximum = 4;
            this.trackBar_gant.Minimum = 1;
            this.trackBar_gant.Name = "trackBar_gant";
            this.trackBar_gant.Size = new System.Drawing.Size(159, 45);
            this.trackBar_gant.TabIndex = 68;
            this.trackBar_gant.Value = 1;
            // 
            // btn_coli_ccw
            // 
            this.btn_coli_ccw.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btn_coli_ccw.Location = new System.Drawing.Point(93, 65);
            this.btn_coli_ccw.Name = "btn_coli_ccw";
            this.btn_coli_ccw.Size = new System.Drawing.Size(72, 30);
            this.btn_coli_ccw.TabIndex = 58;
            this.btn_coli_ccw.Text = "CCW";
            this.btn_coli_ccw.UseVisualStyleBackColor = true;
            this.btn_coli_ccw.Click += new System.EventHandler(this.button5_Click);
            // 
            // btn_x1_out
            // 
            this.btn_x1_out.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btn_x1_out.Location = new System.Drawing.Point(93, 210);
            this.btn_x1_out.Name = "btn_x1_out";
            this.btn_x1_out.Size = new System.Drawing.Size(72, 30);
            this.btn_x1_out.TabIndex = 60;
            this.btn_x1_out.Text = "OUT";
            this.btn_x1_out.UseVisualStyleBackColor = true;
            this.btn_x1_out.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button7_MouseDown);
            // 
            // trackBar_x1
            // 
            this.trackBar_x1.Location = new System.Drawing.Point(181, 210);
            this.trackBar_x1.Maximum = 4;
            this.trackBar_x1.Minimum = 1;
            this.trackBar_x1.Name = "trackBar_x1";
            this.trackBar_x1.Size = new System.Drawing.Size(159, 45);
            this.trackBar_x1.TabIndex = 70;
            this.trackBar_x1.Value = 1;
            // 
            // trackBar_coli
            // 
            this.trackBar_coli.Location = new System.Drawing.Point(181, 65);
            this.trackBar_coli.Maximum = 4;
            this.trackBar_coli.Minimum = 1;
            this.trackBar_coli.Name = "trackBar_coli";
            this.trackBar_coli.Size = new System.Drawing.Size(159, 45);
            this.trackBar_coli.TabIndex = 69;
            this.trackBar_coli.Value = 1;
            // 
            // btn_x1_in
            // 
            this.btn_x1_in.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btn_x1_in.Location = new System.Drawing.Point(4, 210);
            this.btn_x1_in.Name = "btn_x1_in";
            this.btn_x1_in.Size = new System.Drawing.Size(72, 30);
            this.btn_x1_in.TabIndex = 59;
            this.btn_x1_in.Text = "IN";
            this.btn_x1_in.UseVisualStyleBackColor = true;
            this.btn_x1_in.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button6_MouseDown);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabPage2.Controls.Add(this.splitContainer2);
            this.tabPage2.Location = new System.Drawing.Point(4, 27);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1114, 605);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Calibration";
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.btn_cancelLearn);
            this.splitContainer2.Panel1.Controls.Add(this.groupBox4);
            this.splitContainer2.Panel1.Controls.Add(this.btn_edit);
            this.splitContainer2.Panel1.Controls.Add(this.btn_save);
            this.splitContainer2.Panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.splitContainer2.Panel1MinSize = 500;
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.splitContainer2.Panel2.Controls.Add(this.groupBox3);
            this.splitContainer2.Panel2MinSize = 500;
            this.splitContainer2.Size = new System.Drawing.Size(1108, 599);
            this.splitContainer2.SplitterDistance = 581;
            this.splitContainer2.TabIndex = 24;
            // 
            // btn_cancelLearn
            // 
            this.btn_cancelLearn.Enabled = false;
            this.btn_cancelLearn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btn_cancelLearn.Location = new System.Drawing.Point(353, 482);
            this.btn_cancelLearn.Name = "btn_cancelLearn";
            this.btn_cancelLearn.Size = new System.Drawing.Size(77, 30);
            this.btn_cancelLearn.TabIndex = 21;
            this.btn_cancelLearn.Text = "Cancel";
            this.btn_cancelLearn.UseVisualStyleBackColor = true;
            this.btn_cancelLearn.Click += new System.EventHandler(this.btn_cancelLearn_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btn_learn);
            this.groupBox4.Controls.Add(this.comboBox1);
            this.groupBox4.Controls.Add(this.button16);
            this.groupBox4.Controls.Add(this.textBox12);
            this.groupBox4.Controls.Add(this.textBox18);
            this.groupBox4.Controls.Add(this.textBox17);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.groupBox2);
            this.groupBox4.Controls.Add(this.textBox11);
            this.groupBox4.Controls.Add(this.groupBox1);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Enabled = false;
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox4.Location = new System.Drawing.Point(16, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(487, 439);
            this.groupBox4.TabIndex = 22;
            this.groupBox4.TabStop = false;
            // 
            // btn_learn
            // 
            this.btn_learn.Enabled = false;
            this.btn_learn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btn_learn.Location = new System.Drawing.Point(138, 401);
            this.btn_learn.Name = "btn_learn";
            this.btn_learn.Size = new System.Drawing.Size(72, 30);
            this.btn_learn.TabIndex = 20;
            this.btn_learn.Text = "Learn";
            this.btn_learn.UseVisualStyleBackColor = true;
            this.btn_learn.Click += new System.EventHandler(this.btn_learn_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Enabled = false;
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Gantry",
            "Collimator",
            "X1",
            "X2",
            "Y1",
            "Y2"});
            this.comboBox1.Location = new System.Drawing.Point(183, 16);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(105, 26);
            this.comboBox1.TabIndex = 19;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // button16
            // 
            this.button16.Enabled = false;
            this.button16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button16.Location = new System.Drawing.Point(216, 401);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(72, 30);
            this.button16.TabIndex = 18;
            this.button16.Text = "Set Ref";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // textBox12
            // 
            this.textBox12.Enabled = false;
            this.textBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox12.Location = new System.Drawing.Point(183, 86);
            this.textBox12.Name = "textBox12";
            this.textBox12.ReadOnly = true;
            this.textBox12.Size = new System.Drawing.Size(105, 24);
            this.textBox12.TabIndex = 16;
            // 
            // textBox18
            // 
            this.textBox18.Enabled = false;
            this.textBox18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox18.Location = new System.Drawing.Point(183, 358);
            this.textBox18.Name = "textBox18";
            this.textBox18.ReadOnly = true;
            this.textBox18.Size = new System.Drawing.Size(105, 24);
            this.textBox18.TabIndex = 15;
            // 
            // textBox17
            // 
            this.textBox17.Enabled = false;
            this.textBox17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox17.Location = new System.Drawing.Point(183, 328);
            this.textBox17.Name = "textBox17";
            this.textBox17.ReadOnly = true;
            this.textBox17.Size = new System.Drawing.Size(105, 24);
            this.textBox17.TabIndex = 14;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Enabled = false;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label21.Location = new System.Drawing.Point(8, 361);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(169, 18);
            this.label21.TabIndex = 13;
            this.label21.Text = "Calculated Ref Offset";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Enabled = false;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label20.Location = new System.Drawing.Point(8, 331);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(158, 18);
            this.label20.TabIndex = 12;
            this.label20.Text = "Calculated Ref Gain";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox15);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.checkBox2);
            this.groupBox2.Controls.Add(this.textBox16);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Enabled = false;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox2.Location = new System.Drawing.Point(8, 224);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(438, 92);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Step2";
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(175, 51);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(105, 24);
            this.textBox15.TabIndex = 12;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(286, 54);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 18);
            this.label19.TabIndex = 11;
            this.label19.Text = "deg";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(345, 54);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(49, 22);
            this.checkBox2.TabIndex = 10;
            this.checkBox2.Text = "Ok";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(175, 21);
            this.textBox16.Name = "textBox16";
            this.textBox16.ReadOnly = true;
            this.textBox16.Size = new System.Drawing.Size(105, 24);
            this.textBox16.TabIndex = 8;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 24);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(109, 18);
            this.label14.TabIndex = 6;
            this.label14.Text = "Display Value";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(8, 54);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(128, 18);
            this.label17.TabIndex = 7;
            this.label17.Text = "Measured Value";
            // 
            // textBox11
            // 
            this.textBox11.Enabled = false;
            this.textBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox11.Location = new System.Drawing.Point(183, 56);
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.Size = new System.Drawing.Size(105, 24);
            this.textBox11.TabIndex = 9;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox14);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.textBox13);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Enabled = false;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox1.Location = new System.Drawing.Point(8, 126);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(438, 92);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Step1";
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(175, 51);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(105, 24);
            this.textBox14.TabIndex = 12;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(286, 54);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(35, 18);
            this.label18.TabIndex = 11;
            this.label18.Text = "deg";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(345, 54);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(49, 22);
            this.checkBox1.TabIndex = 10;
            this.checkBox1.Text = "Ok";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(175, 21);
            this.textBox13.Name = "textBox13";
            this.textBox13.ReadOnly = true;
            this.textBox13.Size = new System.Drawing.Size(105, 24);
            this.textBox13.TabIndex = 8;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, 24);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(109, 18);
            this.label15.TabIndex = 6;
            this.label15.Text = "Display Value";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(8, 54);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(128, 18);
            this.label16.TabIndex = 7;
            this.label16.Text = "Measured Value";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Enabled = false;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label13.Location = new System.Drawing.Point(8, 89);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(146, 18);
            this.label13.TabIndex = 3;
            this.label13.Text = "Current Ref Offset";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Enabled = false;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label12.Location = new System.Drawing.Point(8, 59);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(135, 18);
            this.label12.TabIndex = 2;
            this.label12.Text = "Current Ref Gain";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Enabled = false;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label11.Location = new System.Drawing.Point(8, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(125, 18);
            this.label11.TabIndex = 1;
            this.label11.Text = "Calibration Part";
            // 
            // btn_edit
            // 
            this.btn_edit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btn_edit.Location = new System.Drawing.Point(353, 450);
            this.btn_edit.Name = "btn_edit";
            this.btn_edit.Size = new System.Drawing.Size(160, 30);
            this.btn_edit.TabIndex = 19;
            this.btn_edit.Text = "Edit";
            this.btn_edit.UseVisualStyleBackColor = true;
            this.btn_edit.Click += new System.EventHandler(this.button17_Click);
            // 
            // btn_save
            // 
            this.btn_save.Enabled = false;
            this.btn_save.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btn_save.Location = new System.Drawing.Point(436, 482);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(77, 30);
            this.btn_save.TabIndex = 20;
            this.btn_save.Text = "Save";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.button18_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tb_coli_flen);
            this.groupBox3.Controls.Add(this.tb_coli_len);
            this.groupBox3.Controls.Add(this.tb_coli_zpnt);
            this.groupBox3.Controls.Add(this.label69);
            this.groupBox3.Controls.Add(this.label68);
            this.groupBox3.Controls.Add(this.tb_gant_flen);
            this.groupBox3.Controls.Add(this.tb_gant_len);
            this.groupBox3.Controls.Add(this.groupBox13);
            this.groupBox3.Controls.Add(this.tb_y2_offset);
            this.groupBox3.Controls.Add(this.tb_gant_zpnt);
            this.groupBox3.Controls.Add(this.tb_y1_offset);
            this.groupBox3.Controls.Add(this.tb_x2_offset);
            this.groupBox3.Controls.Add(this.label65);
            this.groupBox3.Controls.Add(this.tb_x1_offset);
            this.groupBox3.Controls.Add(this.tb_coli_offset);
            this.groupBox3.Controls.Add(this.label66);
            this.groupBox3.Controls.Add(this.tb_gant_offset);
            this.groupBox3.Controls.Add(this.tb_y2_gain);
            this.groupBox3.Controls.Add(this.label67);
            this.groupBox3.Controls.Add(this.tb_y1_gain);
            this.groupBox3.Controls.Add(this.tb_x2_gain);
            this.groupBox3.Controls.Add(this.tb_x1_gain);
            this.groupBox3.Controls.Add(this.tb_coli_gain);
            this.groupBox3.Controls.Add(this.tb_gant_gain);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Location = new System.Drawing.Point(24, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(429, 591);
            this.groupBox3.TabIndex = 23;
            this.groupBox3.TabStop = false;
            // 
            // tb_coli_flen
            // 
            this.tb_coli_flen.BackColor = System.Drawing.SystemColors.Window;
            this.tb_coli_flen.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_coli_flen.Location = new System.Drawing.Point(323, 325);
            this.tb_coli_flen.Name = "tb_coli_flen";
            this.tb_coli_flen.ReadOnly = true;
            this.tb_coli_flen.Size = new System.Drawing.Size(72, 24);
            this.tb_coli_flen.TabIndex = 50;
            this.tb_coli_flen.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // tb_coli_len
            // 
            this.tb_coli_len.BackColor = System.Drawing.SystemColors.Window;
            this.tb_coli_len.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_coli_len.Location = new System.Drawing.Point(246, 325);
            this.tb_coli_len.Name = "tb_coli_len";
            this.tb_coli_len.ReadOnly = true;
            this.tb_coli_len.Size = new System.Drawing.Size(72, 24);
            this.tb_coli_len.TabIndex = 49;
            this.tb_coli_len.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // tb_coli_zpnt
            // 
            this.tb_coli_zpnt.BackColor = System.Drawing.SystemColors.Window;
            this.tb_coli_zpnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_coli_zpnt.Location = new System.Drawing.Point(167, 325);
            this.tb_coli_zpnt.Name = "tb_coli_zpnt";
            this.tb_coli_zpnt.ReadOnly = true;
            this.tb_coli_zpnt.Size = new System.Drawing.Size(72, 24);
            this.tb_coli_zpnt.TabIndex = 48;
            this.tb_coli_zpnt.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label69.Location = new System.Drawing.Point(28, 328);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(86, 18);
            this.label69.TabIndex = 47;
            this.label69.Text = "Collimator";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label68.Location = new System.Drawing.Point(318, 277);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(83, 17);
            this.label68.TabIndex = 46;
            this.label68.Text = "Fine Length";
            // 
            // tb_gant_flen
            // 
            this.tb_gant_flen.BackColor = System.Drawing.SystemColors.Window;
            this.tb_gant_flen.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_gant_flen.Location = new System.Drawing.Point(323, 295);
            this.tb_gant_flen.Name = "tb_gant_flen";
            this.tb_gant_flen.ReadOnly = true;
            this.tb_gant_flen.Size = new System.Drawing.Size(72, 24);
            this.tb_gant_flen.TabIndex = 45;
            this.tb_gant_flen.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // tb_gant_len
            // 
            this.tb_gant_len.BackColor = System.Drawing.SystemColors.Window;
            this.tb_gant_len.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_gant_len.Location = new System.Drawing.Point(246, 295);
            this.tb_gant_len.Name = "tb_gant_len";
            this.tb_gant_len.ReadOnly = true;
            this.tb_gant_len.Size = new System.Drawing.Size(72, 24);
            this.tb_gant_len.TabIndex = 44;
            this.tb_gant_len.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.label45);
            this.groupBox13.Controls.Add(this.textBox53);
            this.groupBox13.Controls.Add(this.textBox54);
            this.groupBox13.Controls.Add(this.label44);
            this.groupBox13.Controls.Add(this.textBox51);
            this.groupBox13.Controls.Add(this.textBox52);
            this.groupBox13.Controls.Add(this.label43);
            this.groupBox13.Controls.Add(this.label41);
            this.groupBox13.Controls.Add(this.label42);
            this.groupBox13.Controls.Add(this.textBox50);
            this.groupBox13.Controls.Add(this.textBox49);
            this.groupBox13.Controls.Add(this.textBox48);
            this.groupBox13.Controls.Add(this.textBox47);
            this.groupBox13.Controls.Add(this.textBox46);
            this.groupBox13.Controls.Add(this.textBox45);
            this.groupBox13.Location = new System.Drawing.Point(13, 383);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(397, 202);
            this.groupBox13.TabIndex = 34;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Couch";
            this.groupBox13.Visible = false;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label45.Location = new System.Drawing.Point(15, 138);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(103, 18);
            this.label45.TabIndex = 33;
            this.label45.Text = "Column Rot.";
            // 
            // textBox53
            // 
            this.textBox53.BackColor = System.Drawing.SystemColors.Window;
            this.textBox53.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox53.Location = new System.Drawing.Point(154, 138);
            this.textBox53.Name = "textBox53";
            this.textBox53.ReadOnly = true;
            this.textBox53.Size = new System.Drawing.Size(105, 24);
            this.textBox53.TabIndex = 32;
            this.textBox53.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // textBox54
            // 
            this.textBox54.BackColor = System.Drawing.SystemColors.Window;
            this.textBox54.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox54.Location = new System.Drawing.Point(277, 138);
            this.textBox54.Name = "textBox54";
            this.textBox54.ReadOnly = true;
            this.textBox54.Size = new System.Drawing.Size(105, 24);
            this.textBox54.TabIndex = 31;
            this.textBox54.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label44.Location = new System.Drawing.Point(15, 173);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(68, 18);
            this.label44.TabIndex = 30;
            this.label44.Text = "Iso Rot.";
            // 
            // textBox51
            // 
            this.textBox51.BackColor = System.Drawing.SystemColors.Window;
            this.textBox51.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox51.Location = new System.Drawing.Point(154, 174);
            this.textBox51.Name = "textBox51";
            this.textBox51.ReadOnly = true;
            this.textBox51.Size = new System.Drawing.Size(105, 24);
            this.textBox51.TabIndex = 29;
            this.textBox51.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // textBox52
            // 
            this.textBox52.BackColor = System.Drawing.SystemColors.Window;
            this.textBox52.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox52.Location = new System.Drawing.Point(277, 174);
            this.textBox52.Name = "textBox52";
            this.textBox52.ReadOnly = true;
            this.textBox52.Size = new System.Drawing.Size(105, 24);
            this.textBox52.TabIndex = 28;
            this.textBox52.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label43.Location = new System.Drawing.Point(15, 33);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(110, 18);
            this.label43.TabIndex = 27;
            this.label43.Text = "Table Vertical";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label41.Location = new System.Drawing.Point(15, 103);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(91, 18);
            this.label41.TabIndex = 20;
            this.label41.Text = "Table Long";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label42.Location = new System.Drawing.Point(15, 68);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(105, 18);
            this.label42.TabIndex = 26;
            this.label42.Text = "Table Lateral";
            // 
            // textBox50
            // 
            this.textBox50.BackColor = System.Drawing.SystemColors.Window;
            this.textBox50.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox50.Location = new System.Drawing.Point(277, 29);
            this.textBox50.Name = "textBox50";
            this.textBox50.ReadOnly = true;
            this.textBox50.Size = new System.Drawing.Size(105, 24);
            this.textBox50.TabIndex = 25;
            this.textBox50.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // textBox49
            // 
            this.textBox49.BackColor = System.Drawing.SystemColors.Window;
            this.textBox49.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox49.Location = new System.Drawing.Point(154, 29);
            this.textBox49.Name = "textBox49";
            this.textBox49.ReadOnly = true;
            this.textBox49.Size = new System.Drawing.Size(105, 24);
            this.textBox49.TabIndex = 24;
            this.textBox49.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // textBox48
            // 
            this.textBox48.BackColor = System.Drawing.SystemColors.Window;
            this.textBox48.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox48.Location = new System.Drawing.Point(277, 102);
            this.textBox48.Name = "textBox48";
            this.textBox48.ReadOnly = true;
            this.textBox48.Size = new System.Drawing.Size(105, 24);
            this.textBox48.TabIndex = 23;
            this.textBox48.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // textBox47
            // 
            this.textBox47.BackColor = System.Drawing.SystemColors.Window;
            this.textBox47.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox47.Location = new System.Drawing.Point(154, 102);
            this.textBox47.Name = "textBox47";
            this.textBox47.ReadOnly = true;
            this.textBox47.Size = new System.Drawing.Size(105, 24);
            this.textBox47.TabIndex = 22;
            this.textBox47.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // textBox46
            // 
            this.textBox46.BackColor = System.Drawing.SystemColors.Window;
            this.textBox46.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox46.Location = new System.Drawing.Point(277, 66);
            this.textBox46.Name = "textBox46";
            this.textBox46.ReadOnly = true;
            this.textBox46.Size = new System.Drawing.Size(105, 24);
            this.textBox46.TabIndex = 21;
            this.textBox46.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // textBox45
            // 
            this.textBox45.BackColor = System.Drawing.SystemColors.Window;
            this.textBox45.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox45.Location = new System.Drawing.Point(154, 66);
            this.textBox45.Name = "textBox45";
            this.textBox45.ReadOnly = true;
            this.textBox45.Size = new System.Drawing.Size(105, 24);
            this.textBox45.TabIndex = 20;
            this.textBox45.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // tb_y2_offset
            // 
            this.tb_y2_offset.BackColor = System.Drawing.SystemColors.Window;
            this.tb_y2_offset.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_y2_offset.Location = new System.Drawing.Point(286, 159);
            this.tb_y2_offset.Name = "tb_y2_offset";
            this.tb_y2_offset.ReadOnly = true;
            this.tb_y2_offset.Size = new System.Drawing.Size(105, 24);
            this.tb_y2_offset.TabIndex = 17;
            this.tb_y2_offset.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // tb_gant_zpnt
            // 
            this.tb_gant_zpnt.BackColor = System.Drawing.SystemColors.Window;
            this.tb_gant_zpnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_gant_zpnt.Location = new System.Drawing.Point(167, 295);
            this.tb_gant_zpnt.Name = "tb_gant_zpnt";
            this.tb_gant_zpnt.ReadOnly = true;
            this.tb_gant_zpnt.Size = new System.Drawing.Size(72, 24);
            this.tb_gant_zpnt.TabIndex = 43;
            this.tb_gant_zpnt.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // tb_y1_offset
            // 
            this.tb_y1_offset.BackColor = System.Drawing.SystemColors.Window;
            this.tb_y1_offset.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_y1_offset.Location = new System.Drawing.Point(286, 118);
            this.tb_y1_offset.Name = "tb_y1_offset";
            this.tb_y1_offset.ReadOnly = true;
            this.tb_y1_offset.Size = new System.Drawing.Size(105, 24);
            this.tb_y1_offset.TabIndex = 16;
            this.tb_y1_offset.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // tb_x2_offset
            // 
            this.tb_x2_offset.BackColor = System.Drawing.SystemColors.Window;
            this.tb_x2_offset.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_x2_offset.Location = new System.Drawing.Point(285, 241);
            this.tb_x2_offset.Name = "tb_x2_offset";
            this.tb_x2_offset.ReadOnly = true;
            this.tb_x2_offset.Size = new System.Drawing.Size(105, 24);
            this.tb_x2_offset.TabIndex = 19;
            this.tb_x2_offset.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label65.Location = new System.Drawing.Point(28, 298);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(58, 18);
            this.label65.TabIndex = 42;
            this.label65.Text = "Gantry";
            // 
            // tb_x1_offset
            // 
            this.tb_x1_offset.BackColor = System.Drawing.SystemColors.Window;
            this.tb_x1_offset.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_x1_offset.Location = new System.Drawing.Point(285, 200);
            this.tb_x1_offset.Name = "tb_x1_offset";
            this.tb_x1_offset.ReadOnly = true;
            this.tb_x1_offset.Size = new System.Drawing.Size(105, 24);
            this.tb_x1_offset.TabIndex = 18;
            this.tb_x1_offset.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // tb_coli_offset
            // 
            this.tb_coli_offset.BackColor = System.Drawing.SystemColors.Window;
            this.tb_coli_offset.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_coli_offset.Location = new System.Drawing.Point(286, 77);
            this.tb_coli_offset.Name = "tb_coli_offset";
            this.tb_coli_offset.ReadOnly = true;
            this.tb_coli_offset.Size = new System.Drawing.Size(105, 24);
            this.tb_coli_offset.TabIndex = 15;
            this.tb_coli_offset.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label66.Location = new System.Drawing.Point(254, 277);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(52, 17);
            this.label66.TabIndex = 41;
            this.label66.Text = "Length";
            // 
            // tb_gant_offset
            // 
            this.tb_gant_offset.BackColor = System.Drawing.SystemColors.Window;
            this.tb_gant_offset.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_gant_offset.Location = new System.Drawing.Point(286, 36);
            this.tb_gant_offset.Name = "tb_gant_offset";
            this.tb_gant_offset.ReadOnly = true;
            this.tb_gant_offset.Size = new System.Drawing.Size(105, 24);
            this.tb_gant_offset.TabIndex = 14;
            this.tb_gant_offset.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // tb_y2_gain
            // 
            this.tb_y2_gain.BackColor = System.Drawing.SystemColors.Window;
            this.tb_y2_gain.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_y2_gain.Location = new System.Drawing.Point(167, 159);
            this.tb_y2_gain.Name = "tb_y2_gain";
            this.tb_y2_gain.ReadOnly = true;
            this.tb_y2_gain.Size = new System.Drawing.Size(105, 24);
            this.tb_y2_gain.TabIndex = 11;
            this.tb_y2_gain.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label67.Location = new System.Drawing.Point(176, 277);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(53, 17);
            this.label67.TabIndex = 40;
            this.label67.Text = "Z Point";
            // 
            // tb_y1_gain
            // 
            this.tb_y1_gain.BackColor = System.Drawing.SystemColors.Window;
            this.tb_y1_gain.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_y1_gain.Location = new System.Drawing.Point(167, 118);
            this.tb_y1_gain.Name = "tb_y1_gain";
            this.tb_y1_gain.ReadOnly = true;
            this.tb_y1_gain.Size = new System.Drawing.Size(105, 24);
            this.tb_y1_gain.TabIndex = 10;
            this.tb_y1_gain.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // tb_x2_gain
            // 
            this.tb_x2_gain.BackColor = System.Drawing.SystemColors.Window;
            this.tb_x2_gain.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_x2_gain.Location = new System.Drawing.Point(166, 241);
            this.tb_x2_gain.Name = "tb_x2_gain";
            this.tb_x2_gain.ReadOnly = true;
            this.tb_x2_gain.Size = new System.Drawing.Size(105, 24);
            this.tb_x2_gain.TabIndex = 13;
            this.tb_x2_gain.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // tb_x1_gain
            // 
            this.tb_x1_gain.BackColor = System.Drawing.SystemColors.Window;
            this.tb_x1_gain.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_x1_gain.Location = new System.Drawing.Point(166, 200);
            this.tb_x1_gain.Name = "tb_x1_gain";
            this.tb_x1_gain.ReadOnly = true;
            this.tb_x1_gain.Size = new System.Drawing.Size(105, 24);
            this.tb_x1_gain.TabIndex = 12;
            this.tb_x1_gain.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // tb_coli_gain
            // 
            this.tb_coli_gain.BackColor = System.Drawing.SystemColors.Window;
            this.tb_coli_gain.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_coli_gain.Location = new System.Drawing.Point(167, 77);
            this.tb_coli_gain.Name = "tb_coli_gain";
            this.tb_coli_gain.ReadOnly = true;
            this.tb_coli_gain.Size = new System.Drawing.Size(105, 24);
            this.tb_coli_gain.TabIndex = 9;
            this.tb_coli_gain.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // tb_gant_gain
            // 
            this.tb_gant_gain.BackColor = System.Drawing.SystemColors.Window;
            this.tb_gant_gain.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_gant_gain.Location = new System.Drawing.Point(167, 36);
            this.tb_gant_gain.Name = "tb_gant_gain";
            this.tb_gant_gain.ReadOnly = true;
            this.tb_gant_gain.Size = new System.Drawing.Size(105, 24);
            this.tb_gant_gain.TabIndex = 8;
            this.tb_gant_gain.Enter += new System.EventHandler(this.txtBox_Enter);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label29.Location = new System.Drawing.Point(28, 162);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(113, 18);
            this.label29.TabIndex = 7;
            this.label29.Text = "Diaphragm Y2";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label28.Location = new System.Drawing.Point(28, 121);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(113, 18);
            this.label28.TabIndex = 6;
            this.label28.Text = "Diaphragm Y1";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label27.Location = new System.Drawing.Point(27, 244);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(114, 18);
            this.label27.TabIndex = 5;
            this.label27.Text = "Diaphragm X2";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label26.Location = new System.Drawing.Point(27, 203);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(114, 18);
            this.label26.TabIndex = 4;
            this.label26.Text = "Diaphragm X1";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label25.Location = new System.Drawing.Point(28, 80);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(132, 18);
            this.label25.TabIndex = 3;
            this.label25.Text = "Collimator Angle";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label24.Location = new System.Drawing.Point(28, 39);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(104, 18);
            this.label24.TabIndex = 2;
            this.label24.Text = "Gantry Angle";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label23.Location = new System.Drawing.Point(307, 15);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(48, 18);
            this.label23.TabIndex = 1;
            this.label23.Text = "Offset";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label22.Location = new System.Drawing.Point(196, 15);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(39, 18);
            this.label22.TabIndex = 0;
            this.label22.Text = "Gain";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabPage3.Controls.Add(this.splitContainer4);
            this.tabPage3.Location = new System.Drawing.Point(4, 27);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1114, 605);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Parameters";
            // 
            // splitContainer4
            // 
            this.splitContainer4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.IsSplitterFixed = true;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.groupBox8);
            this.splitContainer4.Panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.splitContainer4.Panel1MinSize = 400;
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.splitContainer4.Panel2.Controls.Add(this.btn_clearTerminal_oth);
            this.splitContainer4.Panel2.Controls.Add(this.label64);
            this.splitContainer4.Panel2.Controls.Add(this.lbl_in_cnt);
            this.splitContainer4.Panel2.Controls.Add(this.btn_clearTerminal_in);
            this.splitContainer4.Panel2.Controls.Add(this.tb_terminal_oth);
            this.splitContainer4.Panel2.Controls.Add(this.label51);
            this.splitContainer4.Panel2.Controls.Add(this.tb_terminal_in);
            this.splitContainer4.Panel2.Controls.Add(this.btn_clearTerminal);
            this.splitContainer4.Panel2.Controls.Add(this.label48);
            this.splitContainer4.Panel2.Controls.Add(this.tb_terminal_out);
            this.splitContainer4.Panel2.Controls.Add(this.gb_parameters);
            this.splitContainer4.Panel2.Controls.Add(this.lbl_out_cnt);
            this.splitContainer4.Size = new System.Drawing.Size(1114, 605);
            this.splitContainer4.SplitterDistance = 499;
            this.splitContainer4.TabIndex = 30;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.pb_receiveStatus);
            this.groupBox8.Controls.Add(this.label72);
            this.groupBox8.Controls.Add(this.label36);
            this.groupBox8.Controls.Add(this.textBox33);
            this.groupBox8.Controls.Add(this.pictureBox6);
            this.groupBox8.Controls.Add(this.tb_x1_set);
            this.groupBox8.Controls.Add(this.textBox34);
            this.groupBox8.Controls.Add(this.label32);
            this.groupBox8.Controls.Add(this.pictureBox3);
            this.groupBox8.Controls.Add(this.pictureBox5);
            this.groupBox8.Controls.Add(this.pictureBox4);
            this.groupBox8.Controls.Add(this.label30);
            this.groupBox8.Controls.Add(this.textBox35);
            this.groupBox8.Controls.Add(this.label31);
            this.groupBox8.Controls.Add(this.tb_x2_set);
            this.groupBox8.Controls.Add(this.textBox36);
            this.groupBox8.Controls.Add(this.pictureBox2);
            this.groupBox8.Controls.Add(this.label33);
            this.groupBox8.Controls.Add(this.pictureBox1);
            this.groupBox8.Controls.Add(this.textBox31);
            this.groupBox8.Controls.Add(this.textBox32);
            this.groupBox8.Controls.Add(this.tb_coli_set);
            this.groupBox8.Controls.Add(this.tb_y1_set);
            this.groupBox8.Controls.Add(this.tb_gant_set);
            this.groupBox8.Controls.Add(this.label37);
            this.groupBox8.Controls.Add(this.label34);
            this.groupBox8.Controls.Add(this.label35);
            this.groupBox8.Controls.Add(this.tb_y2_set);
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox8.Location = new System.Drawing.Point(3, 15);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(411, 334);
            this.groupBox8.TabIndex = 28;
            this.groupBox8.TabStop = false;
            // 
            // pb_receiveStatus
            // 
            this.pb_receiveStatus.BackgroundImage = global::Compact_Control.Properties.Resources.led_red;
            this.pb_receiveStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pb_receiveStatus.Location = new System.Drawing.Point(359, 293);
            this.pb_receiveStatus.Name = "pb_receiveStatus";
            this.pb_receiveStatus.Size = new System.Drawing.Size(25, 25);
            this.pb_receiveStatus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_receiveStatus.TabIndex = 125;
            this.pb_receiveStatus.TabStop = false;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.BackColor = System.Drawing.Color.Transparent;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label72.ForeColor = System.Drawing.Color.Black;
            this.label72.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label72.Location = new System.Drawing.Point(314, 290);
            this.label72.Margin = new System.Windows.Forms.Padding(0);
            this.label72.Name = "label72";
            this.label72.Padding = new System.Windows.Forms.Padding(5, 5, 0, 0);
            this.label72.Size = new System.Drawing.Size(48, 22);
            this.label72.TabIndex = 126;
            this.label72.Text = "Input:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label36.Location = new System.Drawing.Point(228, 22);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(33, 18);
            this.label36.TabIndex = 12;
            this.label36.Text = "Set";
            // 
            // textBox33
            // 
            this.textBox33.BackColor = System.Drawing.SystemColors.Control;
            this.textBox33.Enabled = false;
            this.textBox33.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox33.Location = new System.Drawing.Point(304, 211);
            this.textBox33.Name = "textBox33";
            this.textBox33.ReadOnly = true;
            this.textBox33.Size = new System.Drawing.Size(80, 24);
            this.textBox33.TabIndex = 8;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox6.Location = new System.Drawing.Point(175, 168);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(24, 24);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 26;
            this.pictureBox6.TabStop = false;
            // 
            // tb_x1_set
            // 
            this.tb_x1_set.BackColor = System.Drawing.SystemColors.Window;
            this.tb_x1_set.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_x1_set.Location = new System.Drawing.Point(207, 211);
            this.tb_x1_set.Name = "tb_x1_set";
            this.tb_x1_set.Size = new System.Drawing.Size(80, 24);
            this.tb_x1_set.TabIndex = 5;
            this.tb_x1_set.TextChanged += new System.EventHandler(this.tb_x1_set_TextChanged);
            this.tb_x1_set.Enter += new System.EventHandler(this.txtBox_Enter);
            this.tb_x1_set.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_x1_set_KeyPress);
            // 
            // textBox34
            // 
            this.textBox34.BackColor = System.Drawing.SystemColors.Control;
            this.textBox34.Enabled = false;
            this.textBox34.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox34.Location = new System.Drawing.Point(303, 251);
            this.textBox34.Name = "textBox34";
            this.textBox34.ReadOnly = true;
            this.textBox34.Size = new System.Drawing.Size(80, 24);
            this.textBox34.TabIndex = 9;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label32.Location = new System.Drawing.Point(29, 213);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(114, 18);
            this.label32.TabIndex = 2;
            this.label32.Text = "Diaphragm X1";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox3.Location = new System.Drawing.Point(175, 207);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(24, 24);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 23;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox5.Location = new System.Drawing.Point(175, 129);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(24, 24);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 25;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox4.Location = new System.Drawing.Point(174, 246);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(24, 24);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 24;
            this.pictureBox4.TabStop = false;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label30.Location = new System.Drawing.Point(29, 53);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(104, 18);
            this.label30.TabIndex = 0;
            this.label30.Text = "Gantry Angle";
            // 
            // textBox35
            // 
            this.textBox35.BackColor = System.Drawing.SystemColors.Control;
            this.textBox35.Enabled = false;
            this.textBox35.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox35.Location = new System.Drawing.Point(304, 131);
            this.textBox35.Name = "textBox35";
            this.textBox35.ReadOnly = true;
            this.textBox35.Size = new System.Drawing.Size(80, 24);
            this.textBox35.TabIndex = 10;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label31.Location = new System.Drawing.Point(29, 93);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(132, 18);
            this.label31.TabIndex = 1;
            this.label31.Text = "Collimator Angle";
            // 
            // tb_x2_set
            // 
            this.tb_x2_set.BackColor = System.Drawing.SystemColors.Window;
            this.tb_x2_set.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_x2_set.Location = new System.Drawing.Point(206, 251);
            this.tb_x2_set.Name = "tb_x2_set";
            this.tb_x2_set.Size = new System.Drawing.Size(80, 24);
            this.tb_x2_set.TabIndex = 6;
            this.tb_x2_set.TextChanged += new System.EventHandler(this.tb_x2_set_TextChanged);
            this.tb_x2_set.Enter += new System.EventHandler(this.txtBox_Enter);
            this.tb_x2_set.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_x2_set_KeyPress);
            // 
            // textBox36
            // 
            this.textBox36.BackColor = System.Drawing.SystemColors.Control;
            this.textBox36.Enabled = false;
            this.textBox36.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox36.Location = new System.Drawing.Point(304, 171);
            this.textBox36.Name = "textBox36";
            this.textBox36.ReadOnly = true;
            this.textBox36.Size = new System.Drawing.Size(80, 24);
            this.textBox36.TabIndex = 11;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox2.Location = new System.Drawing.Point(175, 90);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(24, 24);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 22;
            this.pictureBox2.TabStop = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label33.Location = new System.Drawing.Point(28, 253);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(114, 18);
            this.label33.TabIndex = 3;
            this.label33.Text = "Diaphragm X2";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(175, 51);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(24, 24);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // textBox31
            // 
            this.textBox31.BackColor = System.Drawing.SystemColors.Control;
            this.textBox31.Enabled = false;
            this.textBox31.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox31.Location = new System.Drawing.Point(304, 51);
            this.textBox31.Name = "textBox31";
            this.textBox31.ReadOnly = true;
            this.textBox31.Size = new System.Drawing.Size(80, 24);
            this.textBox31.TabIndex = 6;
            // 
            // textBox32
            // 
            this.textBox32.BackColor = System.Drawing.SystemColors.Control;
            this.textBox32.Enabled = false;
            this.textBox32.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox32.Location = new System.Drawing.Point(304, 91);
            this.textBox32.Name = "textBox32";
            this.textBox32.ReadOnly = true;
            this.textBox32.Size = new System.Drawing.Size(80, 24);
            this.textBox32.TabIndex = 7;
            // 
            // tb_coli_set
            // 
            this.tb_coli_set.BackColor = System.Drawing.SystemColors.Window;
            this.tb_coli_set.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_coli_set.Location = new System.Drawing.Point(207, 91);
            this.tb_coli_set.Name = "tb_coli_set";
            this.tb_coli_set.Size = new System.Drawing.Size(80, 24);
            this.tb_coli_set.TabIndex = 2;
            this.tb_coli_set.TextChanged += new System.EventHandler(this.tb_coli_set_TextChanged);
            this.tb_coli_set.Enter += new System.EventHandler(this.txtBox_Enter);
            this.tb_coli_set.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_coli_set_KeyPress);
            // 
            // tb_y1_set
            // 
            this.tb_y1_set.BackColor = System.Drawing.SystemColors.Window;
            this.tb_y1_set.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_y1_set.Location = new System.Drawing.Point(207, 131);
            this.tb_y1_set.Name = "tb_y1_set";
            this.tb_y1_set.Size = new System.Drawing.Size(80, 24);
            this.tb_y1_set.TabIndex = 3;
            this.tb_y1_set.TextChanged += new System.EventHandler(this.tb_y1_set_TextChanged);
            this.tb_y1_set.Enter += new System.EventHandler(this.txtBox_Enter);
            this.tb_y1_set.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_y1_set_KeyPress);
            // 
            // tb_gant_set
            // 
            this.tb_gant_set.BackColor = System.Drawing.SystemColors.Window;
            this.tb_gant_set.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_gant_set.Location = new System.Drawing.Point(207, 51);
            this.tb_gant_set.Name = "tb_gant_set";
            this.tb_gant_set.Size = new System.Drawing.Size(80, 24);
            this.tb_gant_set.TabIndex = 1;
            this.tb_gant_set.TextChanged += new System.EventHandler(this.tb_gant_set_TextChanged);
            this.tb_gant_set.Enter += new System.EventHandler(this.txtBox_Enter);
            this.tb_gant_set.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_gant_set_KeyPress);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label37.Location = new System.Drawing.Point(316, 22);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(54, 18);
            this.label37.TabIndex = 13;
            this.label37.Text = "Actual";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label34.Location = new System.Drawing.Point(29, 133);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(113, 18);
            this.label34.TabIndex = 4;
            this.label34.Text = "Diaphragm Y1";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label35.Location = new System.Drawing.Point(29, 173);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(113, 18);
            this.label35.TabIndex = 5;
            this.label35.Text = "Diaphragm Y2";
            // 
            // tb_y2_set
            // 
            this.tb_y2_set.BackColor = System.Drawing.SystemColors.Window;
            this.tb_y2_set.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_y2_set.Location = new System.Drawing.Point(207, 171);
            this.tb_y2_set.Name = "tb_y2_set";
            this.tb_y2_set.Size = new System.Drawing.Size(80, 24);
            this.tb_y2_set.TabIndex = 4;
            this.tb_y2_set.TextChanged += new System.EventHandler(this.tb_y2_set_TextChanged);
            this.tb_y2_set.Enter += new System.EventHandler(this.txtBox_Enter);
            this.tb_y2_set.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_y2_set_KeyPress);
            // 
            // btn_clearTerminal_oth
            // 
            this.btn_clearTerminal_oth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btn_clearTerminal_oth.Location = new System.Drawing.Point(135, 568);
            this.btn_clearTerminal_oth.Name = "btn_clearTerminal_oth";
            this.btn_clearTerminal_oth.Size = new System.Drawing.Size(80, 30);
            this.btn_clearTerminal_oth.TabIndex = 124;
            this.btn_clearTerminal_oth.Text = "Clear";
            this.btn_clearTerminal_oth.UseVisualStyleBackColor = true;
            this.btn_clearTerminal_oth.Click += new System.EventHandler(this.btn_clearTerminal_oth_Click);
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label64.Location = new System.Drawing.Point(42, 416);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(129, 18);
            this.label64.TabIndex = 123;
            this.label64.Text = "Terminal (other)";
            // 
            // lbl_in_cnt
            // 
            this.lbl_in_cnt.AutoSize = true;
            this.lbl_in_cnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_in_cnt.Location = new System.Drawing.Point(512, 414);
            this.lbl_in_cnt.Name = "lbl_in_cnt";
            this.lbl_in_cnt.Size = new System.Drawing.Size(18, 20);
            this.lbl_in_cnt.TabIndex = 119;
            this.lbl_in_cnt.Text = "0";
            // 
            // btn_clearTerminal_in
            // 
            this.btn_clearTerminal_in.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btn_clearTerminal_in.Location = new System.Drawing.Point(498, 568);
            this.btn_clearTerminal_in.Name = "btn_clearTerminal_in";
            this.btn_clearTerminal_in.Size = new System.Drawing.Size(80, 30);
            this.btn_clearTerminal_in.TabIndex = 47;
            this.btn_clearTerminal_in.Text = "Clear";
            this.btn_clearTerminal_in.UseVisualStyleBackColor = true;
            this.btn_clearTerminal_in.Click += new System.EventHandler(this.btn_clearTerminal_in_Click);
            // 
            // tb_terminal_oth
            // 
            this.tb_terminal_oth.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.tb_terminal_oth.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_terminal_oth.ForeColor = System.Drawing.Color.Cyan;
            this.tb_terminal_oth.Location = new System.Drawing.Point(45, 435);
            this.tb_terminal_oth.Multiline = true;
            this.tb_terminal_oth.Name = "tb_terminal_oth";
            this.tb_terminal_oth.ReadOnly = true;
            this.tb_terminal_oth.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_terminal_oth.Size = new System.Drawing.Size(170, 132);
            this.tb_terminal_oth.TabIndex = 122;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label51.Location = new System.Drawing.Point(404, 413);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(113, 18);
            this.label51.TabIndex = 46;
            this.label51.Text = "Terminal (in) :";
            // 
            // tb_terminal_in
            // 
            this.tb_terminal_in.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.tb_terminal_in.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_terminal_in.ForeColor = System.Drawing.Color.Cyan;
            this.tb_terminal_in.Location = new System.Drawing.Point(408, 435);
            this.tb_terminal_in.Multiline = true;
            this.tb_terminal_in.Name = "tb_terminal_in";
            this.tb_terminal_in.ReadOnly = true;
            this.tb_terminal_in.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_terminal_in.Size = new System.Drawing.Size(170, 132);
            this.tb_terminal_in.TabIndex = 45;
            // 
            // btn_clearTerminal
            // 
            this.btn_clearTerminal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btn_clearTerminal.Location = new System.Drawing.Point(317, 568);
            this.btn_clearTerminal.Name = "btn_clearTerminal";
            this.btn_clearTerminal.Size = new System.Drawing.Size(80, 30);
            this.btn_clearTerminal.TabIndex = 44;
            this.btn_clearTerminal.Text = "Clear";
            this.btn_clearTerminal.UseVisualStyleBackColor = true;
            this.btn_clearTerminal.Click += new System.EventHandler(this.btn_clearTerminal_Click);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label48.Location = new System.Drawing.Point(227, 414);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(124, 18);
            this.label48.TabIndex = 26;
            this.label48.Text = "Terminal (out) :";
            // 
            // tb_terminal_out
            // 
            this.tb_terminal_out.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.tb_terminal_out.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tb_terminal_out.ForeColor = System.Drawing.Color.Cyan;
            this.tb_terminal_out.Location = new System.Drawing.Point(227, 435);
            this.tb_terminal_out.Multiline = true;
            this.tb_terminal_out.Name = "tb_terminal_out";
            this.tb_terminal_out.ReadOnly = true;
            this.tb_terminal_out.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_terminal_out.Size = new System.Drawing.Size(170, 132);
            this.tb_terminal_out.TabIndex = 25;
            this.tb_terminal_out.TextChanged += new System.EventHandler(this.tb_terminal_TextChanged);
            // 
            // gb_parameters
            // 
            this.gb_parameters.Controls.Add(this.textBox3);
            this.gb_parameters.Controls.Add(this.textBox4);
            this.gb_parameters.Controls.Add(this.textBox5);
            this.gb_parameters.Controls.Add(this.textBox6);
            this.gb_parameters.Controls.Add(this.textBox19);
            this.gb_parameters.Controls.Add(this.textBox20);
            this.gb_parameters.Controls.Add(this.label62);
            this.gb_parameters.Controls.Add(this.btn_saveParameters);
            this.gb_parameters.Controls.Add(this.textBox79);
            this.gb_parameters.Controls.Add(this.textBox80);
            this.gb_parameters.Controls.Add(this.textBox81);
            this.gb_parameters.Controls.Add(this.textBox82);
            this.gb_parameters.Controls.Add(this.textBox83);
            this.gb_parameters.Controls.Add(this.textBox84);
            this.gb_parameters.Controls.Add(this.textBox85);
            this.gb_parameters.Controls.Add(this.textBox86);
            this.gb_parameters.Controls.Add(this.textBox87);
            this.gb_parameters.Controls.Add(this.textBox88);
            this.gb_parameters.Controls.Add(this.textBox89);
            this.gb_parameters.Controls.Add(this.textBox90);
            this.gb_parameters.Controls.Add(this.label52);
            this.gb_parameters.Controls.Add(this.label61);
            this.gb_parameters.Controls.Add(this.textBox55);
            this.gb_parameters.Controls.Add(this.textBox56);
            this.gb_parameters.Controls.Add(this.textBox57);
            this.gb_parameters.Controls.Add(this.textBox58);
            this.gb_parameters.Controls.Add(this.textBox59);
            this.gb_parameters.Controls.Add(this.textBox60);
            this.gb_parameters.Controls.Add(this.textBox61);
            this.gb_parameters.Controls.Add(this.textBox62);
            this.gb_parameters.Controls.Add(this.textBox63);
            this.gb_parameters.Controls.Add(this.textBox64);
            this.gb_parameters.Controls.Add(this.textBox77);
            this.gb_parameters.Controls.Add(this.textBox78);
            this.gb_parameters.Controls.Add(this.label49);
            this.gb_parameters.Controls.Add(this.label50);
            this.gb_parameters.Controls.Add(this.textBox65);
            this.gb_parameters.Controls.Add(this.textBox66);
            this.gb_parameters.Controls.Add(this.textBox67);
            this.gb_parameters.Controls.Add(this.textBox68);
            this.gb_parameters.Controls.Add(this.textBox69);
            this.gb_parameters.Controls.Add(this.textBox70);
            this.gb_parameters.Controls.Add(this.textBox71);
            this.gb_parameters.Controls.Add(this.textBox72);
            this.gb_parameters.Controls.Add(this.textBox73);
            this.gb_parameters.Controls.Add(this.textBox74);
            this.gb_parameters.Controls.Add(this.textBox75);
            this.gb_parameters.Controls.Add(this.textBox76);
            this.gb_parameters.Controls.Add(this.label53);
            this.gb_parameters.Controls.Add(this.label54);
            this.gb_parameters.Controls.Add(this.label55);
            this.gb_parameters.Controls.Add(this.label56);
            this.gb_parameters.Controls.Add(this.label57);
            this.gb_parameters.Controls.Add(this.label58);
            this.gb_parameters.Controls.Add(this.label59);
            this.gb_parameters.Controls.Add(this.label60);
            this.gb_parameters.Location = new System.Drawing.Point(3, 15);
            this.gb_parameters.Name = "gb_parameters";
            this.gb_parameters.Size = new System.Drawing.Size(544, 377);
            this.gb_parameters.TabIndex = 24;
            this.gb_parameters.TabStop = false;
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.Window;
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox3.Location = new System.Drawing.Point(129, 255);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(50, 24);
            this.textBox3.TabIndex = 42;
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.SystemColors.Window;
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox4.Location = new System.Drawing.Point(129, 214);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(50, 24);
            this.textBox4.TabIndex = 35;
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.SystemColors.Window;
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox5.Location = new System.Drawing.Point(129, 174);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(50, 24);
            this.textBox5.TabIndex = 28;
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.SystemColors.Window;
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox6.Location = new System.Drawing.Point(129, 133);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(50, 24);
            this.textBox6.TabIndex = 21;
            // 
            // textBox19
            // 
            this.textBox19.BackColor = System.Drawing.SystemColors.Window;
            this.textBox19.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox19.Location = new System.Drawing.Point(129, 94);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(50, 24);
            this.textBox19.TabIndex = 14;
            // 
            // textBox20
            // 
            this.textBox20.BackColor = System.Drawing.SystemColors.Window;
            this.textBox20.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox20.Location = new System.Drawing.Point(129, 53);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(50, 24);
            this.textBox20.TabIndex = 7;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label62.Location = new System.Drawing.Point(131, 32);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(46, 18);
            this.label62.TabIndex = 44;
            this.label62.Text = "Tol -1";
            // 
            // btn_saveParameters
            // 
            this.btn_saveParameters.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btn_saveParameters.Location = new System.Drawing.Point(372, 304);
            this.btn_saveParameters.Name = "btn_saveParameters";
            this.btn_saveParameters.Size = new System.Drawing.Size(150, 30);
            this.btn_saveParameters.TabIndex = 49;
            this.btn_saveParameters.Text = "Save && Send";
            this.btn_saveParameters.UseVisualStyleBackColor = true;
            this.btn_saveParameters.Click += new System.EventHandler(this.btn_saveParameters_Click);
            // 
            // textBox79
            // 
            this.textBox79.BackColor = System.Drawing.SystemColors.Window;
            this.textBox79.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox79.Location = new System.Drawing.Point(471, 255);
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new System.Drawing.Size(50, 24);
            this.textBox79.TabIndex = 48;
            this.textBox79.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox79.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_V);
            // 
            // textBox80
            // 
            this.textBox80.BackColor = System.Drawing.SystemColors.Window;
            this.textBox80.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox80.Location = new System.Drawing.Point(471, 214);
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new System.Drawing.Size(50, 24);
            this.textBox80.TabIndex = 41;
            this.textBox80.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox80.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_V);
            // 
            // textBox81
            // 
            this.textBox81.BackColor = System.Drawing.SystemColors.Window;
            this.textBox81.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox81.Location = new System.Drawing.Point(471, 174);
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new System.Drawing.Size(50, 24);
            this.textBox81.TabIndex = 34;
            this.textBox81.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox81.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_V);
            // 
            // textBox82
            // 
            this.textBox82.BackColor = System.Drawing.SystemColors.Window;
            this.textBox82.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox82.Location = new System.Drawing.Point(471, 133);
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new System.Drawing.Size(50, 24);
            this.textBox82.TabIndex = 27;
            this.textBox82.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox82.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_V);
            // 
            // textBox83
            // 
            this.textBox83.BackColor = System.Drawing.SystemColors.Window;
            this.textBox83.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox83.Location = new System.Drawing.Point(471, 94);
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new System.Drawing.Size(50, 24);
            this.textBox83.TabIndex = 20;
            this.textBox83.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox83.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_V);
            // 
            // textBox84
            // 
            this.textBox84.BackColor = System.Drawing.SystemColors.Window;
            this.textBox84.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox84.Location = new System.Drawing.Point(471, 53);
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new System.Drawing.Size(50, 24);
            this.textBox84.TabIndex = 13;
            this.textBox84.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox84.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_V);
            // 
            // textBox85
            // 
            this.textBox85.BackColor = System.Drawing.SystemColors.Window;
            this.textBox85.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox85.Location = new System.Drawing.Point(413, 255);
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new System.Drawing.Size(50, 24);
            this.textBox85.TabIndex = 47;
            this.textBox85.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox85.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_V);
            // 
            // textBox86
            // 
            this.textBox86.BackColor = System.Drawing.SystemColors.Window;
            this.textBox86.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox86.Location = new System.Drawing.Point(413, 214);
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new System.Drawing.Size(50, 24);
            this.textBox86.TabIndex = 40;
            this.textBox86.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox86.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_V);
            // 
            // textBox87
            // 
            this.textBox87.BackColor = System.Drawing.SystemColors.Window;
            this.textBox87.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox87.Location = new System.Drawing.Point(413, 174);
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new System.Drawing.Size(50, 24);
            this.textBox87.TabIndex = 33;
            this.textBox87.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox87.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_V);
            // 
            // textBox88
            // 
            this.textBox88.BackColor = System.Drawing.SystemColors.Window;
            this.textBox88.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox88.Location = new System.Drawing.Point(413, 133);
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new System.Drawing.Size(50, 24);
            this.textBox88.TabIndex = 26;
            this.textBox88.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox88.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_V);
            // 
            // textBox89
            // 
            this.textBox89.BackColor = System.Drawing.SystemColors.Window;
            this.textBox89.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox89.Location = new System.Drawing.Point(413, 94);
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new System.Drawing.Size(50, 24);
            this.textBox89.TabIndex = 19;
            this.textBox89.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox89.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_V);
            // 
            // textBox90
            // 
            this.textBox90.BackColor = System.Drawing.SystemColors.Window;
            this.textBox90.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox90.Location = new System.Drawing.Point(413, 53);
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new System.Drawing.Size(50, 24);
            this.textBox90.TabIndex = 12;
            this.textBox90.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox90.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_V);
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label52.Location = new System.Drawing.Point(482, 32);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(29, 18);
            this.label52.TabIndex = 36;
            this.label52.Text = "V 3";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label61.Location = new System.Drawing.Point(424, 32);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(29, 18);
            this.label61.TabIndex = 35;
            this.label61.Text = "V 2";
            // 
            // textBox55
            // 
            this.textBox55.BackColor = System.Drawing.SystemColors.Window;
            this.textBox55.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox55.Location = new System.Drawing.Point(357, 255);
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new System.Drawing.Size(50, 24);
            this.textBox55.TabIndex = 46;
            this.textBox55.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox55.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_V);
            // 
            // textBox56
            // 
            this.textBox56.BackColor = System.Drawing.SystemColors.Window;
            this.textBox56.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox56.Location = new System.Drawing.Point(357, 214);
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new System.Drawing.Size(50, 24);
            this.textBox56.TabIndex = 39;
            this.textBox56.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox56.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_V);
            // 
            // textBox57
            // 
            this.textBox57.BackColor = System.Drawing.SystemColors.Window;
            this.textBox57.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox57.Location = new System.Drawing.Point(357, 174);
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new System.Drawing.Size(50, 24);
            this.textBox57.TabIndex = 32;
            this.textBox57.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox57.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_V);
            // 
            // textBox58
            // 
            this.textBox58.BackColor = System.Drawing.SystemColors.Window;
            this.textBox58.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox58.Location = new System.Drawing.Point(357, 133);
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new System.Drawing.Size(50, 24);
            this.textBox58.TabIndex = 25;
            this.textBox58.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox58.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_V);
            // 
            // textBox59
            // 
            this.textBox59.BackColor = System.Drawing.SystemColors.Window;
            this.textBox59.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox59.Location = new System.Drawing.Point(357, 94);
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new System.Drawing.Size(50, 24);
            this.textBox59.TabIndex = 18;
            this.textBox59.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox59.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_V);
            // 
            // textBox60
            // 
            this.textBox60.BackColor = System.Drawing.SystemColors.Window;
            this.textBox60.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox60.Location = new System.Drawing.Point(357, 53);
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new System.Drawing.Size(50, 24);
            this.textBox60.TabIndex = 11;
            this.textBox60.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox60.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_V);
            // 
            // textBox61
            // 
            this.textBox61.BackColor = System.Drawing.SystemColors.Window;
            this.textBox61.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox61.Location = new System.Drawing.Point(299, 255);
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new System.Drawing.Size(50, 24);
            this.textBox61.TabIndex = 45;
            this.textBox61.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox61.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox76_KeyPress);
            this.textBox61.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_tol);
            // 
            // textBox62
            // 
            this.textBox62.BackColor = System.Drawing.SystemColors.Window;
            this.textBox62.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox62.Location = new System.Drawing.Point(299, 214);
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new System.Drawing.Size(50, 24);
            this.textBox62.TabIndex = 38;
            this.textBox62.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox62.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox76_KeyPress);
            this.textBox62.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_tol);
            // 
            // textBox63
            // 
            this.textBox63.BackColor = System.Drawing.SystemColors.Window;
            this.textBox63.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox63.Location = new System.Drawing.Point(299, 174);
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new System.Drawing.Size(50, 24);
            this.textBox63.TabIndex = 31;
            this.textBox63.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox63.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox76_KeyPress);
            this.textBox63.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_tol);
            // 
            // textBox64
            // 
            this.textBox64.BackColor = System.Drawing.SystemColors.Window;
            this.textBox64.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox64.Location = new System.Drawing.Point(299, 133);
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new System.Drawing.Size(50, 24);
            this.textBox64.TabIndex = 24;
            this.textBox64.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox64.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox76_KeyPress);
            this.textBox64.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_tol);
            // 
            // textBox77
            // 
            this.textBox77.BackColor = System.Drawing.SystemColors.Window;
            this.textBox77.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox77.Location = new System.Drawing.Point(299, 94);
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new System.Drawing.Size(50, 24);
            this.textBox77.TabIndex = 17;
            this.textBox77.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox77.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox76_KeyPress);
            this.textBox77.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_tol);
            // 
            // textBox78
            // 
            this.textBox78.BackColor = System.Drawing.SystemColors.Window;
            this.textBox78.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox78.Location = new System.Drawing.Point(299, 53);
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new System.Drawing.Size(50, 24);
            this.textBox78.TabIndex = 10;
            this.textBox78.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox78.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox76_KeyPress);
            this.textBox78.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_tol);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label49.Location = new System.Drawing.Point(368, 32);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(29, 18);
            this.label49.TabIndex = 21;
            this.label49.Text = "V 1";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label50.Location = new System.Drawing.Point(304, 32);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(41, 18);
            this.label50.TabIndex = 20;
            this.label50.Text = "Tol 2";
            // 
            // textBox65
            // 
            this.textBox65.BackColor = System.Drawing.SystemColors.Window;
            this.textBox65.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox65.Location = new System.Drawing.Point(243, 255);
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new System.Drawing.Size(50, 24);
            this.textBox65.TabIndex = 44;
            this.textBox65.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox65.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox76_KeyPress);
            this.textBox65.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_tol);
            // 
            // textBox66
            // 
            this.textBox66.BackColor = System.Drawing.SystemColors.Window;
            this.textBox66.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox66.Location = new System.Drawing.Point(243, 214);
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new System.Drawing.Size(50, 24);
            this.textBox66.TabIndex = 37;
            this.textBox66.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox66.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox76_KeyPress);
            this.textBox66.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_tol);
            // 
            // textBox67
            // 
            this.textBox67.BackColor = System.Drawing.SystemColors.Window;
            this.textBox67.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox67.Location = new System.Drawing.Point(243, 174);
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new System.Drawing.Size(50, 24);
            this.textBox67.TabIndex = 30;
            this.textBox67.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox67.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox76_KeyPress);
            this.textBox67.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_tol);
            // 
            // textBox68
            // 
            this.textBox68.BackColor = System.Drawing.SystemColors.Window;
            this.textBox68.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox68.Location = new System.Drawing.Point(243, 133);
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new System.Drawing.Size(50, 24);
            this.textBox68.TabIndex = 23;
            this.textBox68.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox68.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox76_KeyPress);
            this.textBox68.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_tol);
            // 
            // textBox69
            // 
            this.textBox69.BackColor = System.Drawing.SystemColors.Window;
            this.textBox69.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox69.Location = new System.Drawing.Point(243, 94);
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new System.Drawing.Size(50, 24);
            this.textBox69.TabIndex = 16;
            this.textBox69.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox69.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox76_KeyPress);
            this.textBox69.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_tol);
            // 
            // textBox70
            // 
            this.textBox70.BackColor = System.Drawing.SystemColors.Window;
            this.textBox70.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox70.Location = new System.Drawing.Point(243, 53);
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new System.Drawing.Size(50, 24);
            this.textBox70.TabIndex = 9;
            this.textBox70.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox70.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox76_KeyPress);
            this.textBox70.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_tol);
            // 
            // textBox71
            // 
            this.textBox71.BackColor = System.Drawing.SystemColors.Window;
            this.textBox71.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox71.Location = new System.Drawing.Point(185, 255);
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new System.Drawing.Size(50, 24);
            this.textBox71.TabIndex = 43;
            this.textBox71.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox71.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox76_KeyPress);
            this.textBox71.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_tol);
            // 
            // textBox72
            // 
            this.textBox72.BackColor = System.Drawing.SystemColors.Window;
            this.textBox72.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox72.Location = new System.Drawing.Point(185, 214);
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new System.Drawing.Size(50, 24);
            this.textBox72.TabIndex = 36;
            this.textBox72.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox72.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox76_KeyPress);
            this.textBox72.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_tol);
            // 
            // textBox73
            // 
            this.textBox73.BackColor = System.Drawing.SystemColors.Window;
            this.textBox73.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox73.Location = new System.Drawing.Point(185, 174);
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new System.Drawing.Size(50, 24);
            this.textBox73.TabIndex = 29;
            this.textBox73.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox73.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox76_KeyPress);
            this.textBox73.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_tol);
            // 
            // textBox74
            // 
            this.textBox74.BackColor = System.Drawing.SystemColors.Window;
            this.textBox74.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox74.Location = new System.Drawing.Point(185, 133);
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new System.Drawing.Size(50, 24);
            this.textBox74.TabIndex = 22;
            this.textBox74.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox74.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox76_KeyPress);
            this.textBox74.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_tol);
            // 
            // textBox75
            // 
            this.textBox75.BackColor = System.Drawing.SystemColors.Window;
            this.textBox75.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox75.Location = new System.Drawing.Point(185, 94);
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new System.Drawing.Size(50, 24);
            this.textBox75.TabIndex = 15;
            this.textBox75.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox75.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox76_KeyPress);
            this.textBox75.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_tol);
            // 
            // textBox76
            // 
            this.textBox76.BackColor = System.Drawing.SystemColors.Window;
            this.textBox76.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textBox76.Location = new System.Drawing.Point(185, 53);
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new System.Drawing.Size(50, 24);
            this.textBox76.TabIndex = 8;
            this.textBox76.Enter += new System.EventHandler(this.txtBox_Enter);
            this.textBox76.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox76_KeyPress);
            this.textBox76.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_Text_tol);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label53.Location = new System.Drawing.Point(39, 258);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(68, 18);
            this.label53.TabIndex = 7;
            this.label53.Text = "X2 (cm)";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label54.Location = new System.Drawing.Point(39, 217);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(68, 18);
            this.label54.TabIndex = 6;
            this.label54.Text = "X1 (cm)";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label55.Location = new System.Drawing.Point(39, 177);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(67, 18);
            this.label55.TabIndex = 5;
            this.label55.Text = "Y2 (cm)";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label56.Location = new System.Drawing.Point(39, 136);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(67, 18);
            this.label56.TabIndex = 4;
            this.label56.Text = "Y1 (cm)";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label57.Location = new System.Drawing.Point(39, 97);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(86, 18);
            this.label57.TabIndex = 3;
            this.label57.Text = "Collimator";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label58.Location = new System.Drawing.Point(39, 56);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(58, 18);
            this.label58.TabIndex = 2;
            this.label58.Text = "Gantry";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label59.Location = new System.Drawing.Point(247, 32);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(41, 18);
            this.label59.TabIndex = 1;
            this.label59.Text = "Tol 1";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label60.Location = new System.Drawing.Point(190, 32);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(41, 18);
            this.label60.TabIndex = 0;
            this.label60.Text = "Tol 0";
            // 
            // lbl_out_cnt
            // 
            this.lbl_out_cnt.AutoSize = true;
            this.lbl_out_cnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_out_cnt.Location = new System.Drawing.Point(347, 414);
            this.lbl_out_cnt.Name = "lbl_out_cnt";
            this.lbl_out_cnt.Size = new System.Drawing.Size(18, 20);
            this.lbl_out_cnt.TabIndex = 118;
            this.lbl_out_cnt.Text = "0";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Error2.png");
            this.imageList1.Images.SetKeyName(1, "Request2.png");
            // 
            // panel_AdminControls
            // 
            this.panel_AdminControls.BackColor = System.Drawing.SystemColors.Control;
            this.panel_AdminControls.Controls.Add(this.tabControl1);
            this.panel_AdminControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_AdminControls.Enabled = false;
            this.panel_AdminControls.Location = new System.Drawing.Point(0, 103);
            this.panel_AdminControls.Name = "panel_AdminControls";
            this.panel_AdminControls.Size = new System.Drawing.Size(1122, 653);
            this.panel_AdminControls.TabIndex = 2;
            // 
            // panel_Toolbar
            // 
            this.panel_Toolbar.BackColor = System.Drawing.SystemColors.Control;
            this.panel_Toolbar.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel_Toolbar.Controls.Add(this.panel3);
            this.panel_Toolbar.Controls.Add(this.panel2);
            this.panel_Toolbar.Controls.Add(this.picBtn_Exit);
            this.panel_Toolbar.Controls.Add(this.picBtn_LogOff);
            this.panel_Toolbar.Controls.Add(this.picBtn_Setting);
            this.panel_Toolbar.Controls.Add(this.splitter2);
            this.panel_Toolbar.Controls.Add(this.picBtn_Restart);
            this.panel_Toolbar.Controls.Add(this.splitter3);
            this.panel_Toolbar.Controls.Add(this.picBtn_Shutdown);
            this.panel_Toolbar.Controls.Add(this.splitter1);
            this.panel_Toolbar.Controls.Add(this.picBtn_Connect);
            this.panel_Toolbar.Controls.Add(this.pictureBox14);
            this.panel_Toolbar.Controls.Add(this.panel1);
            this.panel_Toolbar.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_Toolbar.Location = new System.Drawing.Point(0, 0);
            this.panel_Toolbar.Name = "panel_Toolbar";
            this.panel_Toolbar.Size = new System.Drawing.Size(1122, 103);
            this.panel_Toolbar.TabIndex = 3;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel3.Location = new System.Drawing.Point(240, 68);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(646, 32);
            this.panel3.TabIndex = 24;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel2.Location = new System.Drawing.Point(240, 36);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(646, 32);
            this.panel2.TabIndex = 20;
            // 
            // picBtn_Exit
            // 
            this.picBtn_Exit.BackgroundImage = global::Compact_Control.Properties.Resources.Exit2;
            this.picBtn_Exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picBtn_Exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picBtn_Exit.Dock = System.Windows.Forms.DockStyle.Left;
            this.picBtn_Exit.Location = new System.Drawing.Point(160, 36);
            this.picBtn_Exit.Name = "picBtn_Exit";
            this.picBtn_Exit.Size = new System.Drawing.Size(80, 63);
            this.picBtn_Exit.TabIndex = 11;
            this.picBtn_Exit.TabStop = false;
            this.picBtnToolTip.SetToolTip(this.picBtn_Exit, " Exit");
            this.picBtn_Exit.Click += new System.EventHandler(this.picBtn_Exit_Click);
            this.picBtn_Exit.MouseEnter += new System.EventHandler(this.picBtn_Exit_MouseEnter);
            this.picBtn_Exit.MouseLeave += new System.EventHandler(this.picBtn_Exit_MouseLeave);
            // 
            // picBtn_LogOff
            // 
            this.picBtn_LogOff.BackgroundImage = global::Compact_Control.Properties.Resources.Logout;
            this.picBtn_LogOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picBtn_LogOff.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picBtn_LogOff.Dock = System.Windows.Forms.DockStyle.Left;
            this.picBtn_LogOff.Location = new System.Drawing.Point(90, 36);
            this.picBtn_LogOff.Name = "picBtn_LogOff";
            this.picBtn_LogOff.Size = new System.Drawing.Size(70, 63);
            this.picBtn_LogOff.TabIndex = 9;
            this.picBtn_LogOff.TabStop = false;
            this.picBtnToolTip.SetToolTip(this.picBtn_LogOff, " Log out");
            this.picBtn_LogOff.Click += new System.EventHandler(this.picBtn_LogOff_Click);
            this.picBtn_LogOff.MouseEnter += new System.EventHandler(this.picBtn_LogOff_MouseEnter);
            this.picBtn_LogOff.MouseLeave += new System.EventHandler(this.picBtn_LogOff_MouseLeave);
            // 
            // picBtn_Setting
            // 
            this.picBtn_Setting.BackgroundImage = global::Compact_Control.Properties.Resources.Setting;
            this.picBtn_Setting.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picBtn_Setting.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picBtn_Setting.Dock = System.Windows.Forms.DockStyle.Right;
            this.picBtn_Setting.Location = new System.Drawing.Point(886, 36);
            this.picBtn_Setting.Name = "picBtn_Setting";
            this.picBtn_Setting.Size = new System.Drawing.Size(64, 63);
            this.picBtn_Setting.TabIndex = 10;
            this.picBtn_Setting.TabStop = false;
            this.picBtnToolTip.SetToolTip(this.picBtn_Setting, "Settings");
            this.picBtn_Setting.Click += new System.EventHandler(this.picBtn_Setting_Click);
            this.picBtn_Setting.MouseEnter += new System.EventHandler(this.picBtn_Setting_MouseEnter);
            this.picBtn_Setting.MouseLeave += new System.EventHandler(this.picBtn_Setting_MouseLeave);
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(950, 36);
            this.splitter2.Margin = new System.Windows.Forms.Padding(2);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(11, 63);
            this.splitter2.TabIndex = 16;
            this.splitter2.TabStop = false;
            // 
            // picBtn_Restart
            // 
            this.picBtn_Restart.BackgroundImage = global::Compact_Control.Properties.Resources.Restart;
            this.picBtn_Restart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picBtn_Restart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picBtn_Restart.Dock = System.Windows.Forms.DockStyle.Right;
            this.picBtn_Restart.Location = new System.Drawing.Point(961, 36);
            this.picBtn_Restart.Name = "picBtn_Restart";
            this.picBtn_Restart.Size = new System.Drawing.Size(64, 63);
            this.picBtn_Restart.TabIndex = 12;
            this.picBtn_Restart.TabStop = false;
            this.picBtnToolTip.SetToolTip(this.picBtn_Restart, "Restart Computer");
            this.picBtn_Restart.Click += new System.EventHandler(this.picBtn_Restart_Click);
            this.picBtn_Restart.MouseEnter += new System.EventHandler(this.picBtn_Restart_MouseEnter);
            this.picBtn_Restart.MouseLeave += new System.EventHandler(this.picBtn_Restart_MouseLeave);
            // 
            // splitter3
            // 
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter3.Location = new System.Drawing.Point(1025, 36);
            this.splitter3.Margin = new System.Windows.Forms.Padding(2);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(11, 63);
            this.splitter3.TabIndex = 17;
            this.splitter3.TabStop = false;
            // 
            // picBtn_Shutdown
            // 
            this.picBtn_Shutdown.BackgroundImage = global::Compact_Control.Properties.Resources.Shutdown;
            this.picBtn_Shutdown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picBtn_Shutdown.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picBtn_Shutdown.Dock = System.Windows.Forms.DockStyle.Right;
            this.picBtn_Shutdown.Location = new System.Drawing.Point(1036, 36);
            this.picBtn_Shutdown.Name = "picBtn_Shutdown";
            this.picBtn_Shutdown.Size = new System.Drawing.Size(71, 63);
            this.picBtn_Shutdown.TabIndex = 13;
            this.picBtn_Shutdown.TabStop = false;
            this.picBtnToolTip.SetToolTip(this.picBtn_Shutdown, "Shutdown Computer");
            this.picBtn_Shutdown.Click += new System.EventHandler(this.picBtn_Shutdown_Click);
            this.picBtn_Shutdown.MouseEnter += new System.EventHandler(this.picBtn_Shutdown_MouseEnter);
            this.picBtn_Shutdown.MouseLeave += new System.EventHandler(this.picBtn_Shutdown_MouseLeave);
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(1107, 36);
            this.splitter1.Margin = new System.Windows.Forms.Padding(2);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(11, 63);
            this.splitter1.TabIndex = 15;
            this.splitter1.TabStop = false;
            // 
            // picBtn_Connect
            // 
            this.picBtn_Connect.BackgroundImage = global::Compact_Control.Properties.Resources.ConnectButton;
            this.picBtn_Connect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picBtn_Connect.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picBtn_Connect.Dock = System.Windows.Forms.DockStyle.Left;
            this.picBtn_Connect.Location = new System.Drawing.Point(11, 36);
            this.picBtn_Connect.Name = "picBtn_Connect";
            this.picBtn_Connect.Size = new System.Drawing.Size(79, 63);
            this.picBtn_Connect.TabIndex = 3;
            this.picBtn_Connect.TabStop = false;
            this.picBtn_Connect.Click += new System.EventHandler(this.picBtn_Connect_Click);
            this.picBtn_Connect.MouseEnter += new System.EventHandler(this.picBtn_Connect_MouseEnter);
            this.picBtn_Connect.MouseLeave += new System.EventHandler(this.picBtn_Connect_MouseLeave);
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox14.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox14.Location = new System.Drawing.Point(0, 36);
            this.pictureBox14.Margin = new System.Windows.Forms.Padding(5);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Padding = new System.Windows.Forms.Padding(2);
            this.pictureBox14.Size = new System.Drawing.Size(11, 63);
            this.pictureBox14.TabIndex = 6;
            this.pictureBox14.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lbl_init);
            this.panel1.Controls.Add(this.label_date);
            this.panel1.Controls.Add(this.label47);
            this.panel1.Controls.Add(this.label_time);
            this.panel1.Controls.Add(this.label_ConnectStatus);
            this.panel1.Controls.Add(this.label_title);
            this.panel1.Controls.Add(this.label70);
            this.panel1.Controls.Add(this.label71);
            this.panel1.Controls.Add(this.picBtn_Close);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1118, 36);
            this.panel1.TabIndex = 14;
            // 
            // lbl_init
            // 
            this.lbl_init.AutoSize = true;
            this.lbl_init.BackColor = System.Drawing.Color.Transparent;
            this.lbl_init.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbl_init.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lbl_init.ForeColor = System.Drawing.Color.Orange;
            this.lbl_init.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_init.Location = new System.Drawing.Point(112, 0);
            this.lbl_init.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_init.Name = "lbl_init";
            this.lbl_init.Padding = new System.Windows.Forms.Padding(5, 8, 10, 0);
            this.lbl_init.Size = new System.Drawing.Size(114, 25);
            this.lbl_init.TabIndex = 18;
            this.lbl_init.Text = "Not Initialized !";
            this.lbl_init.Visible = false;
            // 
            // label_date
            // 
            this.label_date.AutoSize = true;
            this.label_date.Dock = System.Windows.Forms.DockStyle.Right;
            this.label_date.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label_date.Location = new System.Drawing.Point(802, 0);
            this.label_date.Name = "label_date";
            this.label_date.Padding = new System.Windows.Forms.Padding(0, 5, 1, 5);
            this.label_date.Size = new System.Drawing.Size(40, 28);
            this.label_date.TabIndex = 7;
            this.label_date.Text = "Date";
            this.label_date.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Dock = System.Windows.Forms.DockStyle.Right;
            this.label47.Enabled = false;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label47.Location = new System.Drawing.Point(842, 0);
            this.label47.Name = "label47";
            this.label47.Padding = new System.Windows.Forms.Padding(0, 5, 1, 5);
            this.label47.Size = new System.Drawing.Size(15, 28);
            this.label47.TabIndex = 10;
            this.label47.Text = "|";
            // 
            // label_time
            // 
            this.label_time.AutoSize = true;
            this.label_time.Dock = System.Windows.Forms.DockStyle.Right;
            this.label_time.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label_time.Location = new System.Drawing.Point(857, 0);
            this.label_time.Name = "label_time";
            this.label_time.Padding = new System.Windows.Forms.Padding(1, 5, 1, 5);
            this.label_time.Size = new System.Drawing.Size(43, 28);
            this.label_time.TabIndex = 6;
            this.label_time.Text = "Time";
            this.label_time.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label_ConnectStatus
            // 
            this.label_ConnectStatus.AutoSize = true;
            this.label_ConnectStatus.BackColor = System.Drawing.Color.Transparent;
            this.label_ConnectStatus.Dock = System.Windows.Forms.DockStyle.Left;
            this.label_ConnectStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label_ConnectStatus.ForeColor = System.Drawing.Color.Red;
            this.label_ConnectStatus.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label_ConnectStatus.Location = new System.Drawing.Point(0, 0);
            this.label_ConnectStatus.Margin = new System.Windows.Forms.Padding(0);
            this.label_ConnectStatus.Name = "label_ConnectStatus";
            this.label_ConnectStatus.Padding = new System.Windows.Forms.Padding(5, 8, 10, 0);
            this.label_ConnectStatus.Size = new System.Drawing.Size(112, 25);
            this.label_ConnectStatus.TabIndex = 4;
            this.label_ConnectStatus.Text = "Disconnected!";
            // 
            // label_title
            // 
            this.label_title.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label_title.Location = new System.Drawing.Point(0, 0);
            this.label_title.Name = "label_title";
            this.label_title.Padding = new System.Windows.Forms.Padding(70, 0, 0, 0);
            this.label_title.Size = new System.Drawing.Size(900, 34);
            this.label_title.TabIndex = 5;
            this.label_title.Text = "Service";
            this.label_title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Dock = System.Windows.Forms.DockStyle.Right;
            this.label70.Enabled = false;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label70.Location = new System.Drawing.Point(900, 0);
            this.label70.Name = "label70";
            this.label70.Padding = new System.Windows.Forms.Padding(0, 5, 1, 5);
            this.label70.Size = new System.Drawing.Size(15, 28);
            this.label70.TabIndex = 21;
            this.label70.Text = "|";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Dock = System.Windows.Forms.DockStyle.Right;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label71.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label71.Location = new System.Drawing.Point(915, 0);
            this.label71.Name = "label71";
            this.label71.Padding = new System.Windows.Forms.Padding(0, 3, 1, 4);
            this.label71.Size = new System.Drawing.Size(153, 29);
            this.label71.TabIndex = 20;
            this.label71.Text = "Golestan Hospital";
            this.label71.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // picBtn_Close
            // 
            this.picBtn_Close.BackgroundImage = global::Compact_Control.Properties.Resources.Error;
            this.picBtn_Close.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picBtn_Close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picBtn_Close.Dock = System.Windows.Forms.DockStyle.Right;
            this.picBtn_Close.Location = new System.Drawing.Point(1068, 0);
            this.picBtn_Close.Margin = new System.Windows.Forms.Padding(2);
            this.picBtn_Close.Name = "picBtn_Close";
            this.picBtn_Close.Padding = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.picBtn_Close.Size = new System.Drawing.Size(48, 34);
            this.picBtn_Close.TabIndex = 18;
            this.picBtn_Close.TabStop = false;
            this.picBtnToolTip.SetToolTip(this.picBtn_Close, "Exit");
            this.picBtn_Close.Visible = false;
            this.picBtn_Close.Click += new System.EventHandler(this.picBtn_Close_Click);
            // 
            // picBtnToolTip
            // 
            this.picBtnToolTip.AutoPopDelay = 5000;
            this.picBtnToolTip.BackColor = System.Drawing.Color.White;
            this.picBtnToolTip.InitialDelay = 500;
            this.picBtnToolTip.ReshowDelay = 100;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label39.Location = new System.Drawing.Point(102, -88);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(40, 22);
            this.label39.TabIndex = 65;
            this.label39.Text = "Set";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label7.Location = new System.Drawing.Point(199, -88);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 22);
            this.label7.TabIndex = 66;
            this.label7.Text = "Actual";
            // 
            // panel_ClientControls
            // 
            this.panel_ClientControls.Controls.Add(this.label7);
            this.panel_ClientControls.Controls.Add(this.label39);
            this.panel_ClientControls.Enabled = false;
            this.panel_ClientControls.Location = new System.Drawing.Point(969, 94);
            this.panel_ClientControls.Name = "panel_ClientControls";
            this.panel_ClientControls.Size = new System.Drawing.Size(39, 10);
            this.panel_ClientControls.TabIndex = 4;
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick_1);
            // 
            // timer3
            // 
            this.timer3.Interval = 2;
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // timer4
            // 
            this.timer4.Enabled = true;
            this.timer4.Interval = 3000;
            this.timer4.Tick += new System.EventHandler(this.timer4_Tick);
            // 
            // timer5
            // 
            this.timer5.Enabled = true;
            this.timer5.Interval = 200;
            this.timer5.Tick += new System.EventHandler(this.timer5_Tick);
            // 
            // panel_status
            // 
            this.panel_status.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_status.Controls.Add(this.label_upTime);
            this.panel_status.Controls.Add(this.label73);
            this.panel_status.Controls.Add(this.label_ram);
            this.panel_status.Controls.Add(this.lbl_version);
            this.panel_status.Controls.Add(this.label46);
            this.panel_status.Controls.Add(this.label_cpu);
            this.panel_status.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel_status.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel_status.Location = new System.Drawing.Point(0, 756);
            this.panel_status.Name = "panel_status";
            this.panel_status.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.panel_status.Size = new System.Drawing.Size(1122, 32);
            this.panel_status.TabIndex = 27;
            // 
            // label_upTime
            // 
            this.label_upTime.AutoSize = true;
            this.label_upTime.Dock = System.Windows.Forms.DockStyle.Right;
            this.label_upTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label_upTime.Location = new System.Drawing.Point(941, 0);
            this.label_upTime.Name = "label_upTime";
            this.label_upTime.Padding = new System.Windows.Forms.Padding(0, 5, 1, 5);
            this.label_upTime.Size = new System.Drawing.Size(61, 28);
            this.label_upTime.TabIndex = 29;
            this.label_upTime.Text = "UpTime";
            this.label_upTime.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Dock = System.Windows.Forms.DockStyle.Right;
            this.label73.Enabled = false;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label73.Location = new System.Drawing.Point(1002, 0);
            this.label73.Name = "label73";
            this.label73.Padding = new System.Windows.Forms.Padding(0, 5, 1, 5);
            this.label73.Size = new System.Drawing.Size(15, 28);
            this.label73.TabIndex = 30;
            this.label73.Text = "|";
            // 
            // label_ram
            // 
            this.label_ram.AutoSize = true;
            this.label_ram.Dock = System.Windows.Forms.DockStyle.Right;
            this.label_ram.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label_ram.Location = new System.Drawing.Point(1017, 0);
            this.label_ram.Name = "label_ram";
            this.label_ram.Padding = new System.Windows.Forms.Padding(0, 5, 1, 5);
            this.label_ram.Size = new System.Drawing.Size(42, 28);
            this.label_ram.TabIndex = 28;
            this.label_ram.Text = "RAM";
            this.label_ram.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_version
            // 
            this.lbl_version.AutoSize = true;
            this.lbl_version.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbl_version.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lbl_version.Location = new System.Drawing.Point(5, 0);
            this.lbl_version.Name = "lbl_version";
            this.lbl_version.Padding = new System.Windows.Forms.Padding(5, 5, 1, 5);
            this.lbl_version.Size = new System.Drawing.Size(84, 28);
            this.lbl_version.TabIndex = 26;
            this.lbl_version.Text = "Ver. 1.3.22";
            this.lbl_version.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Dock = System.Windows.Forms.DockStyle.Right;
            this.label46.Enabled = false;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label46.Location = new System.Drawing.Point(1059, 0);
            this.label46.Name = "label46";
            this.label46.Padding = new System.Windows.Forms.Padding(0, 5, 1, 5);
            this.label46.Size = new System.Drawing.Size(15, 28);
            this.label46.TabIndex = 31;
            this.label46.Text = "|";
            // 
            // label_cpu
            // 
            this.label_cpu.AutoSize = true;
            this.label_cpu.Dock = System.Windows.Forms.DockStyle.Right;
            this.label_cpu.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label_cpu.Location = new System.Drawing.Point(1074, 0);
            this.label_cpu.Name = "label_cpu";
            this.label_cpu.Padding = new System.Windows.Forms.Padding(0, 5, 1, 5);
            this.label_cpu.Size = new System.Drawing.Size(41, 28);
            this.label_cpu.TabIndex = 27;
            this.label_cpu.Text = "CPU";
            this.label_cpu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1122, 788);
            this.Controls.Add(this.panel_AdminControls);
            this.Controls.Add(this.panel_Toolbar);
            this.Controls.Add(this.panel_ClientControls);
            this.Controls.Add(this.panel_status);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Activated += new System.EventHandler(this.Form1_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.VisibleChanged += new System.EventHandler(this.Form1_VisibleChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_y2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_y1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_x2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_gant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_x1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_coli)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            this.splitContainer4.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_receiveStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.gb_parameters.ResumeLayout(false);
            this.gb_parameters.PerformLayout();
            this.panel_AdminControls.ResumeLayout(false);
            this.panel_Toolbar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picBtn_Exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBtn_LogOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBtn_Setting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBtn_Restart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBtn_Shutdown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBtn_Connect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBtn_Close)).EndInit();
            this.panel_ClientControls.ResumeLayout(false);
            this.panel_ClientControls.PerformLayout();
            this.panel_status.ResumeLayout(false);
            this.panel_status.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btn_start_stop;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox textBox10;
        public System.Windows.Forms.TextBox textBox9;
        public System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TrackBar trackBar_y2;
        private System.Windows.Forms.TrackBar trackBar_y1;
        private System.Windows.Forms.TrackBar trackBar_x2;
        public System.Windows.Forms.TrackBar trackBar_x1;
        private System.Windows.Forms.TrackBar trackBar_coli;
        private System.Windows.Forms.TrackBar trackBar_gant;
        private System.Windows.Forms.Button btn_y2_out;
        private System.Windows.Forms.Button btn_y1_out;
        private System.Windows.Forms.Button btn_y2_in;
        private System.Windows.Forms.Button btn_y1_in;
        private System.Windows.Forms.Button btn_x2_out;
        private System.Windows.Forms.Button btn_x2_in;
        private System.Windows.Forms.Button btn_x1_out;
        private System.Windows.Forms.Button btn_x1_in;
        private System.Windows.Forms.Button btn_coli_ccw;
        private System.Windows.Forms.Button btn_coli_cw;
        private System.Windows.Forms.Button btn_gant_ccw;
        private System.Windows.Forms.Button btn_gant_cw;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox tb_y2_co;
        public System.Windows.Forms.TextBox tb_y1_co;
        public System.Windows.Forms.TextBox tb_x2_co;
        public System.Windows.Forms.TextBox tb_x1_co;
        public System.Windows.Forms.TextBox textBox2;
        public System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        public System.Windows.Forms.TextBox textBox44;
        public System.Windows.Forms.TextBox textBox43;
        public System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.PictureBox picBtn_Connect;
        private System.Windows.Forms.ToolTip picBtnToolTip;
        public System.Windows.Forms.Panel panel_AdminControls;
        public System.Windows.Forms.Panel panel_Toolbar;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.Panel panel_ClientControls;
        private System.Windows.Forms.Label label_ConnectStatus;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.Label label40;
        public System.Windows.Forms.TextBox adcheck;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.PictureBox picBtn_Shutdown;
        public System.Windows.Forms.PictureBox picBtn_Restart;
        public System.Windows.Forms.PictureBox picBtn_Exit;
        public System.Windows.Forms.PictureBox picBtn_Setting;
        public System.Windows.Forms.PictureBox picBtn_LogOff;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Label label_date;
        private System.Windows.Forms.Label label_time;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label label47;
        public System.IO.Ports.SerialPort serialPort1;
        public System.Windows.Forms.PictureBox picBtn_Close;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btn_edit;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox textBox53;
        private System.Windows.Forms.TextBox textBox54;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox textBox51;
        private System.Windows.Forms.TextBox textBox52;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox textBox50;
        private System.Windows.Forms.TextBox textBox49;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.TextBox tb_y2_offset;
        private System.Windows.Forms.TextBox tb_y1_offset;
        private System.Windows.Forms.TextBox tb_x2_offset;
        private System.Windows.Forms.TextBox tb_x1_offset;
        private System.Windows.Forms.TextBox tb_coli_offset;
        private System.Windows.Forms.TextBox tb_gant_offset;
        private System.Windows.Forms.TextBox tb_y2_gain;
        private System.Windows.Forms.TextBox tb_y1_gain;
        private System.Windows.Forms.TextBox tb_x2_gain;
        private System.Windows.Forms.TextBox tb_x1_gain;
        private System.Windows.Forms.TextBox tb_coli_gain;
        private System.Windows.Forms.TextBox tb_gant_gain;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.TextBox tb_x1_set;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox tb_x2_set;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox tb_coli_set;
        private System.Windows.Forms.TextBox tb_y1_set;
        private System.Windows.Forms.TextBox tb_gant_set;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox tb_y2_set;
        private System.Windows.Forms.GroupBox gb_parameters;
        private System.Windows.Forms.Button btn_saveParameters;
        private System.Windows.Forms.TextBox textBox79;
        private System.Windows.Forms.TextBox textBox80;
        private System.Windows.Forms.TextBox textBox81;
        private System.Windows.Forms.TextBox textBox82;
        private System.Windows.Forms.TextBox textBox83;
        private System.Windows.Forms.TextBox textBox84;
        private System.Windows.Forms.TextBox textBox85;
        private System.Windows.Forms.TextBox textBox86;
        private System.Windows.Forms.TextBox textBox87;
        private System.Windows.Forms.TextBox textBox88;
        private System.Windows.Forms.TextBox textBox89;
        private System.Windows.Forms.TextBox textBox90;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox textBox55;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.TextBox textBox60;
        private System.Windows.Forms.TextBox textBox61;
        private System.Windows.Forms.TextBox textBox62;
        private System.Windows.Forms.TextBox textBox63;
        private System.Windows.Forms.TextBox textBox64;
        private System.Windows.Forms.TextBox textBox77;
        private System.Windows.Forms.TextBox textBox78;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox textBox65;
        private System.Windows.Forms.TextBox textBox66;
        private System.Windows.Forms.TextBox textBox67;
        private System.Windows.Forms.TextBox textBox68;
        private System.Windows.Forms.TextBox textBox69;
        private System.Windows.Forms.TextBox textBox70;
        private System.Windows.Forms.TextBox textBox71;
        private System.Windows.Forms.TextBox textBox72;
        private System.Windows.Forms.TextBox textBox73;
        private System.Windows.Forms.TextBox textBox74;
        private System.Windows.Forms.TextBox textBox75;
        private System.Windows.Forms.TextBox textBox76;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        public System.Windows.Forms.Label lbl_init;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox tb_terminal_out;
        private System.Windows.Forms.Button btn_clearTerminal;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox tb_terminal_in;
        private System.Windows.Forms.Button btn_clearTerminal_in;
        private System.Windows.Forms.Label lbl_in_cnt;
        private System.Windows.Forms.Label lbl_out_cnt;
        private System.Windows.Forms.Button btn_clearTerminal_oth;
        private System.Windows.Forms.TextBox tb_terminal_oth;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Button btn_learn;
        private System.Windows.Forms.Timer timer4;
        private System.Windows.Forms.TextBox tb_gant_flen;
        private System.Windows.Forms.TextBox tb_gant_len;
        private System.Windows.Forms.TextBox tb_gant_zpnt;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox tb_coli_flen;
        private System.Windows.Forms.TextBox tb_coli_len;
        private System.Windows.Forms.TextBox tb_coli_zpnt;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Button btn_cancelLearn;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label_title;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.Timer timer5;
        private System.Windows.Forms.PictureBox pb_receiveStatus;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Panel panel_status;
        private System.Windows.Forms.Label lbl_version;
        private System.Windows.Forms.Label label_ram;
        private System.Windows.Forms.Label label_cpu;
        private System.Windows.Forms.Label label_upTime;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label46;
    }
}

