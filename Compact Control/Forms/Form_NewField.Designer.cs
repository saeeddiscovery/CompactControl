﻿namespace Compact_Control
{
    partial class Form_NewField
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox_Field = new System.Windows.Forms.GroupBox();
            this.txt_shadowTray = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_bolous = new System.Windows.Forms.TextBox();
            this.txt_wedge = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_mu = new System.Windows.Forms.TextBox();
            this.txt_dose = new System.Windows.Forms.TextBox();
            this.txt_ssd = new System.Windows.Forms.TextBox();
            this.txt_site = new System.Windows.Forms.TextBox();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox_values = new System.Windows.Forms.GroupBox();
            this.txt_y2 = new System.Windows.Forms.TextBox();
            this.txt_y1 = new System.Windows.Forms.TextBox();
            this.txt_x2 = new System.Windows.Forms.TextBox();
            this.txt_x1 = new System.Windows.Forms.TextBox();
            this.txt_coli = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txt_gant = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.btn_Ok = new System.Windows.Forms.Button();
            this.btn_Clear = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txt_Long = new System.Windows.Forms.TextBox();
            this.txt_Lat = new System.Windows.Forms.TextBox();
            this.txt_Column = new System.Windows.Forms.TextBox();
            this.txt_Vert = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_Iso = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox_Field.SuspendLayout();
            this.groupBox_values.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_Field
            // 
            this.groupBox_Field.Controls.Add(this.txt_shadowTray);
            this.groupBox_Field.Controls.Add(this.label2);
            this.groupBox_Field.Controls.Add(this.txt_bolous);
            this.groupBox_Field.Controls.Add(this.txt_wedge);
            this.groupBox_Field.Controls.Add(this.label1);
            this.groupBox_Field.Controls.Add(this.txt_mu);
            this.groupBox_Field.Controls.Add(this.txt_dose);
            this.groupBox_Field.Controls.Add(this.txt_ssd);
            this.groupBox_Field.Controls.Add(this.txt_site);
            this.groupBox_Field.Controls.Add(this.txt_name);
            this.groupBox_Field.Controls.Add(this.label15);
            this.groupBox_Field.Controls.Add(this.label10);
            this.groupBox_Field.Controls.Add(this.label11);
            this.groupBox_Field.Controls.Add(this.label12);
            this.groupBox_Field.Controls.Add(this.label9);
            this.groupBox_Field.Controls.Add(this.label7);
            this.groupBox_Field.ForeColor = System.Drawing.Color.Black;
            this.groupBox_Field.Location = new System.Drawing.Point(12, 12);
            this.groupBox_Field.Name = "groupBox_Field";
            this.groupBox_Field.Size = new System.Drawing.Size(434, 104);
            this.groupBox_Field.TabIndex = 1;
            this.groupBox_Field.TabStop = false;
            this.groupBox_Field.Text = "Field";
            // 
            // txt_shadowTray
            // 
            this.txt_shadowTray.Location = new System.Drawing.Point(380, 44);
            this.txt_shadowTray.Name = "txt_shadowTray";
            this.txt_shadowTray.Size = new System.Drawing.Size(44, 20);
            this.txt_shadowTray.TabIndex = 74;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(302, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 70;
            this.label2.Text = "Shadow Tray:";
            // 
            // txt_bolous
            // 
            this.txt_bolous.Location = new System.Drawing.Point(380, 69);
            this.txt_bolous.Name = "txt_bolous";
            this.txt_bolous.Size = new System.Drawing.Size(44, 20);
            this.txt_bolous.TabIndex = 69;
            // 
            // txt_wedge
            // 
            this.txt_wedge.FormattingEnabled = true;
            this.txt_wedge.Items.AddRange(new object[] {
            "IN",
            "OUT"});
            this.txt_wedge.Location = new System.Drawing.Point(380, 18);
            this.txt_wedge.Name = "txt_wedge";
            this.txt_wedge.Size = new System.Drawing.Size(44, 21);
            this.txt_wedge.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(302, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "BOLOUS (mm):";
            // 
            // txt_mu
            // 
            this.txt_mu.Location = new System.Drawing.Point(211, 71);
            this.txt_mu.Name = "txt_mu";
            this.txt_mu.Size = new System.Drawing.Size(72, 20);
            this.txt_mu.TabIndex = 15;
            // 
            // txt_dose
            // 
            this.txt_dose.Location = new System.Drawing.Point(211, 44);
            this.txt_dose.Name = "txt_dose";
            this.txt_dose.Size = new System.Drawing.Size(72, 20);
            this.txt_dose.TabIndex = 14;
            // 
            // txt_ssd
            // 
            this.txt_ssd.Location = new System.Drawing.Point(211, 18);
            this.txt_ssd.Name = "txt_ssd";
            this.txt_ssd.Size = new System.Drawing.Size(72, 20);
            this.txt_ssd.TabIndex = 13;
            // 
            // txt_site
            // 
            this.txt_site.Location = new System.Drawing.Point(65, 44);
            this.txt_site.Name = "txt_site";
            this.txt_site.Size = new System.Drawing.Size(72, 20);
            this.txt_site.TabIndex = 12;
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(65, 18);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(72, 20);
            this.txt_name.TabIndex = 11;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(302, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(51, 13);
            this.label15.TabIndex = 8;
            this.label15.Text = "WEDGE:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(152, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "MU:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(152, 47);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "DOSE (Gr):";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(152, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "SSD (cm):";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 45);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "SITE:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Name:";
            // 
            // groupBox_values
            // 
            this.groupBox_values.Controls.Add(this.txt_y2);
            this.groupBox_values.Controls.Add(this.txt_y1);
            this.groupBox_values.Controls.Add(this.txt_x2);
            this.groupBox_values.Controls.Add(this.txt_x1);
            this.groupBox_values.Controls.Add(this.txt_coli);
            this.groupBox_values.Controls.Add(this.label35);
            this.groupBox_values.Controls.Add(this.txt_gant);
            this.groupBox_values.Controls.Add(this.label34);
            this.groupBox_values.Controls.Add(this.label33);
            this.groupBox_values.Controls.Add(this.label32);
            this.groupBox_values.Controls.Add(this.label31);
            this.groupBox_values.Controls.Add(this.label30);
            this.groupBox_values.Location = new System.Drawing.Point(12, 210);
            this.groupBox_values.Name = "groupBox_values";
            this.groupBox_values.Size = new System.Drawing.Size(434, 83);
            this.groupBox_values.TabIndex = 70;
            this.groupBox_values.TabStop = false;
            this.groupBox_values.Text = "Set Values";
            // 
            // txt_y2
            // 
            this.txt_y2.Location = new System.Drawing.Point(341, 53);
            this.txt_y2.Name = "txt_y2";
            this.txt_y2.Size = new System.Drawing.Size(72, 20);
            this.txt_y2.TabIndex = 68;
            // 
            // txt_y1
            // 
            this.txt_y1.Location = new System.Drawing.Point(341, 26);
            this.txt_y1.Name = "txt_y1";
            this.txt_y1.Size = new System.Drawing.Size(72, 20);
            this.txt_y1.TabIndex = 67;
            // 
            // txt_x2
            // 
            this.txt_x2.Location = new System.Drawing.Point(212, 53);
            this.txt_x2.Name = "txt_x2";
            this.txt_x2.Size = new System.Drawing.Size(72, 20);
            this.txt_x2.TabIndex = 66;
            // 
            // txt_x1
            // 
            this.txt_x1.Location = new System.Drawing.Point(212, 26);
            this.txt_x1.Name = "txt_x1";
            this.txt_x1.Size = new System.Drawing.Size(72, 20);
            this.txt_x1.TabIndex = 65;
            // 
            // txt_coli
            // 
            this.txt_coli.Location = new System.Drawing.Point(84, 52);
            this.txt_coli.Name = "txt_coli";
            this.txt_coli.Size = new System.Drawing.Size(72, 20);
            this.txt_coli.TabIndex = 20;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label35.Location = new System.Drawing.Point(184, 26);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(22, 15);
            this.label35.TabIndex = 64;
            this.label35.Text = "X1";
            // 
            // txt_gant
            // 
            this.txt_gant.Location = new System.Drawing.Point(84, 25);
            this.txt_gant.Name = "txt_gant";
            this.txt_gant.Size = new System.Drawing.Size(72, 20);
            this.txt_gant.TabIndex = 19;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label34.Location = new System.Drawing.Point(184, 53);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(22, 15);
            this.label34.TabIndex = 63;
            this.label34.Text = "X2";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label33.Location = new System.Drawing.Point(314, 26);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(21, 15);
            this.label33.TabIndex = 62;
            this.label33.Text = "Y1";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label32.Location = new System.Drawing.Point(314, 53);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(21, 15);
            this.label32.TabIndex = 61;
            this.label32.Text = "Y2";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label31.Location = new System.Drawing.Point(15, 53);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(63, 15);
            this.label31.TabIndex = 60;
            this.label31.Text = "Collimator";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label30.Location = new System.Drawing.Point(15, 26);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(42, 15);
            this.label30.TabIndex = 59;
            this.label30.Text = "Gantry";
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(290, 307);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(75, 23);
            this.btn_Cancel.TabIndex = 72;
            this.btn_Cancel.Text = "Cancel";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // btn_Ok
            // 
            this.btn_Ok.Location = new System.Drawing.Point(371, 307);
            this.btn_Ok.Name = "btn_Ok";
            this.btn_Ok.Size = new System.Drawing.Size(75, 23);
            this.btn_Ok.TabIndex = 71;
            this.btn_Ok.Text = "OK";
            this.btn_Ok.UseVisualStyleBackColor = true;
            this.btn_Ok.Click += new System.EventHandler(this.btn_Ok_Click);
            // 
            // btn_Clear
            // 
            this.btn_Clear.Location = new System.Drawing.Point(12, 307);
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.Size = new System.Drawing.Size(75, 23);
            this.btn_Clear.TabIndex = 73;
            this.btn_Clear.Text = "Clear";
            this.btn_Clear.UseVisualStyleBackColor = true;
            this.btn_Clear.Click += new System.EventHandler(this.btn_Clear_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txt_Long);
            this.groupBox1.Controls.Add(this.txt_Lat);
            this.groupBox1.Controls.Add(this.txt_Column);
            this.groupBox1.Controls.Add(this.txt_Vert);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txt_Iso);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Location = new System.Drawing.Point(12, 122);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(434, 83);
            this.groupBox1.TabIndex = 71;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Couch";
            // 
            // txt_Long
            // 
            this.txt_Long.Location = new System.Drawing.Point(341, 53);
            this.txt_Long.Name = "txt_Long";
            this.txt_Long.Size = new System.Drawing.Size(72, 20);
            this.txt_Long.TabIndex = 68;
            // 
            // txt_Lat
            // 
            this.txt_Lat.Location = new System.Drawing.Point(212, 53);
            this.txt_Lat.Name = "txt_Lat";
            this.txt_Lat.Size = new System.Drawing.Size(72, 20);
            this.txt_Lat.TabIndex = 66;
            // 
            // txt_Column
            // 
            this.txt_Column.Location = new System.Drawing.Point(212, 26);
            this.txt_Column.Name = "txt_Column";
            this.txt_Column.Size = new System.Drawing.Size(72, 20);
            this.txt_Column.TabIndex = 65;
            // 
            // txt_Vert
            // 
            this.txt_Vert.Location = new System.Drawing.Point(65, 52);
            this.txt_Vert.Name = "txt_Vert";
            this.txt_Vert.Size = new System.Drawing.Size(72, 20);
            this.txt_Vert.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label4.Location = new System.Drawing.Point(152, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 15);
            this.label4.TabIndex = 64;
            this.label4.Text = "Column:";
            // 
            // txt_Iso
            // 
            this.txt_Iso.Location = new System.Drawing.Point(65, 25);
            this.txt_Iso.Name = "txt_Iso";
            this.txt_Iso.Size = new System.Drawing.Size(72, 20);
            this.txt_Iso.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label5.Location = new System.Drawing.Point(152, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 15);
            this.label5.TabIndex = 63;
            this.label5.Text = "Lat:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label8.Location = new System.Drawing.Point(302, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 15);
            this.label8.TabIndex = 61;
            this.label8.Text = "Long:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label13.Location = new System.Drawing.Point(15, 53);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 15);
            this.label13.TabIndex = 60;
            this.label13.Text = "Vert:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label14.Location = new System.Drawing.Point(15, 26);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(26, 15);
            this.label14.TabIndex = 59;
            this.label14.Text = "Iso:";
            // 
            // Form_NewField
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(461, 342);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_Clear);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Ok);
            this.Controls.Add(this.groupBox_values);
            this.Controls.Add(this.groupBox_Field);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form_NewField";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form_NewField";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_NewField_FormClosing);
            this.Load += new System.EventHandler(this.Form_NewField_Load);
            this.groupBox_Field.ResumeLayout(false);
            this.groupBox_Field.PerformLayout();
            this.groupBox_values.ResumeLayout(false);
            this.groupBox_values.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_Field;
        private System.Windows.Forms.ComboBox txt_wedge;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_mu;
        private System.Windows.Forms.TextBox txt_dose;
        private System.Windows.Forms.TextBox txt_ssd;
        private System.Windows.Forms.TextBox txt_site;
        private System.Windows.Forms.TextBox txt_name;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txt_bolous;
        private System.Windows.Forms.GroupBox groupBox_values;
        private System.Windows.Forms.TextBox txt_y2;
        private System.Windows.Forms.TextBox txt_y1;
        private System.Windows.Forms.TextBox txt_x2;
        private System.Windows.Forms.TextBox txt_x1;
        private System.Windows.Forms.TextBox txt_coli;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txt_gant;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Button btn_Ok;
        private System.Windows.Forms.Button btn_Clear;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_shadowTray;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txt_Long;
        private System.Windows.Forms.TextBox txt_Lat;
        private System.Windows.Forms.TextBox txt_Column;
        private System.Windows.Forms.TextBox txt_Vert;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_Iso;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
    }
}